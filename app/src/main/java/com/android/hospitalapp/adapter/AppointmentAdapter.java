package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.Appointment;
import com.android.hospitalapp.model.DateHeader;
import com.android.hospitalapp.model.Item;

import java.util.Calendar;
import java.util.List;

public class AppointmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_APPOINTMENT = 1;

    private Context context;
    private List<Item> items;
    private AppointmentListener listener;
    private boolean isPatient;

    public AppointmentAdapter(Context context, List<Item> items, AppointmentListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
        this.isPatient = false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_APPOINTMENT) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_schedule_v2, parent, false);
            return new AppointmentViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_header, parent, false);
            return new HeaderViewHolder(view);
        }

        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (items.get(position) instanceof Appointment) {
            Appointment appointment = (Appointment) items.get(position);
            AppointmentViewHolder holder = (AppointmentViewHolder) viewHolder;

            if (isPatient) {
                holder.txtTitle.setText(appointment.getStaffName() + " - " + appointment.getStaffPhone());
            } else {
                holder.txtTitle.setText(appointment.getPatientName() + " - " + appointment.getPatientPhone());
            }
            if (appointment.getTimeDisplay() != null) {
                String time = appointment.getTimeDisplay();
                if (!appointment.getTimeDisplay().contains(" - ")) {
                    time = time.replaceAll("-", " - ");
                }
                holder.txtDetail.setText(time.replaceAll(":00:00", ":00"));
            }

            holder.itemView.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemClick(appointment);
                }
            });
        } else if (items.get(position) instanceof DateHeader) {
            DateHeader header = (DateHeader) items.get(position);
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            if (isPatient) {
                holder.txtName.setText(header.getDate());
            } else {
                try {
                    Calendar calendar = Calendar.getInstance();
                    String[] strings = header.getDate().split("-");
                    calendar.set(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]) - 1, Integer.parseInt(strings[2]));

                    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                        holder.txtName.setText("Chủ nhật");
                    } else {
                        holder.txtName.setText("Thứ " + calendar.get(Calendar.DAY_OF_WEEK));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof Appointment) {
            return TYPE_APPOINTMENT;
        }

        return TYPE_HEADER;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setPatient(boolean isPatient) {
        this.isPatient = isPatient;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
        }
    }

    public class AppointmentViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle, txtDetail;

        public AppointmentViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.txt_title);
            txtDetail = itemView.findViewById(R.id.txt_detail);
        }
    }

    public interface AppointmentListener {

        void onItemClick(Appointment appointment);
    }
}
