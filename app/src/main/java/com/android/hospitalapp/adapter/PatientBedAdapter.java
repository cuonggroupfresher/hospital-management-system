package com.android.hospitalapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.Bed;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PatientBedAdapter extends RecyclerView.Adapter<PatientBedAdapter.ViewHolder> {

    private static final String TAG = PatientBedAdapter.class.getName();

    private Context context;
    private List<Bed> beds;
    private PatientBedAdapter.PatientBedListener listener;

    public PatientBedAdapter(Context context, List<Bed> beds, PatientBedAdapter.PatientBedListener listener) {
        this.context = context;
        this.beds = beds;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PatientBedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_patient_bed, parent, false);
        return new PatientBedAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientBedAdapter.ViewHolder holder, int position) {
        Bed bed = beds.get(position);

        String date = bed.getDateIn().replace("T", " ");
        date = date.substring(0, date.lastIndexOf("."));

        holder.txtID.setText(bed.getPatientIDUser());
        holder.txtName.setText(bed.getPatientName());
        holder.txtDateIn.setText(date);
        holder.image.setImageResource(R.drawable.ic_patient);
        holder.txtBed.setText(bed.getName());
        holder.txtRoom.setText("Phòng " + bed.getRoomNumber());
        holder.txtGender.setText(bed.getGender());

        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClick(bed);
            }
        });
    }

    @Override
    public int getItemCount() {
        return beds.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image;
        private TextView txtID, txtName, txtDateIn, txtGender, txtBed, txtRoom;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            txtID = itemView.findViewById(R.id.txt_id);
            txtName = itemView.findViewById(R.id.txt_name);
            txtDateIn = itemView.findViewById(R.id.txt_birthday);
            txtGender = itemView.findViewById(R.id.txt_gender);
            txtBed = itemView.findViewById(R.id.txt_bed);
            txtRoom = itemView.findViewById(R.id.txt_room);
        }
    }

    public interface PatientBedListener {

        void onItemClick(Bed bed);
    }
}
