package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class StaffArrayAdapter extends ArrayAdapter<Staff> {

    private final Context context;
    private final List<Staff> staffs;
    private List<Staff> filteredList;
    private final int layoutResourceID;

    public StaffArrayAdapter(@NonNull Context context, int resource, List<Staff> staffs) {
        super(context, resource, staffs);

        this.context = context;
        this.layoutResourceID = resource;
        this.staffs = new ArrayList<>(staffs);
        this.filteredList = new ArrayList<>(staffs);
    }

    public int getCount() {
        return filteredList.size();
    }

    public Staff getItem(int position) {
        return filteredList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceID, parent, false);
            }

            Staff staff = getItem(position);
            TextView name = convertView.findViewById(R.id.textView);
            name.setText(staff.getId() + " - " + staff.getFullname());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();

                List<Staff> suggestions = new ArrayList<>();
                if (charSequence == null || charSequence.length() == 0) {
                    suggestions.addAll(staffs);
                } else {
                    String filterString = StringUtils.removeAccent(charSequence.toString().toLowerCase().trim());

                    for (Staff staff : staffs) {
                        if (StringUtils.removeAccent(staff.getFullname()).toLowerCase().contains(filterString) ||
                                StringUtils.removeAccent(staff.getId()).toLowerCase().contains(filterString)) {
                            Log.d("StaffArrayAdapter", "performFiltering: " + staff);
                            suggestions.add(staff);
                        }
                    }
                }

                results.values = suggestions;
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (List<Staff>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
