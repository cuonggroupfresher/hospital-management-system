package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.DateHeader;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Schedule;

import java.util.List;

public classScheduleV2Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_SCHEDULE = 1;

    private Context context;
    private List<Item> items;
    private ScheduleListener listener;

    public ScheduleV2Adapter(Context context, List<Item> items, ScheduleListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_SCHEDULE) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_schedule_v2, parent, false);
            return new ScheduleViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_header, parent, false);
            return new HeaderViewHolder(view);
        }

        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (items.get(position) instanceof Schedule) {
            Schedule schedule = (Schedule) items.get(position);
            ScheduleViewHolder holder = (ScheduleViewHolder) viewHolder;


            String title = "";
            // if (schedule.getRoleID().equals("2")) {
            //     title = "Bác sĩ ";
            // } else if (schedule.getRoleID().equals("3")) {
            //     title = "Y tá ";
            // }

            holder.txtTitle.setText(schedule.getUserID() + " - " + title + schedule.getName() + " - " + schedule.getPhoneNumber());
            if (schedule.getDescription() == null || schedule.getDescription().isEmpty()) {
                holder.txtDetail.setVisibility(View.GONE);
            } else {
                holder.txtDetail.setVisibility(View.GONE);
                holder.txtDetail.setText("- " + schedule.getDescription());
            }

            holder.itemView.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemClick(schedule);
                }
            });
        } else if (items.get(position) instanceof DateHeader) {
            DateHeader header = (DateHeader) items.get(position);
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.txtName.setText("Ngày " + header.getDate().substring(header.getDate().lastIndexOf("-") + 1));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof Schedule) {
            return TYPE_SCHEDULE;
        }

        return TYPE_HEADER;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
        }
    }

    public class ScheduleViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle, txtDetail;

        public ScheduleViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.txt_title);
            txtDetail = itemView.findViewById(R.id.txt_detail);
        }
    }

    public interface ScheduleListener {

        void onItemClick(Schedule schedule);
    }
}
