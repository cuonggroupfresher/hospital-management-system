package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Bed;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Person;
import com.android.hospitalapp.model.Room;
import com.android.hospitalapp.model.Staff;
import com.google.gson.Gson;

import java.util.List;

public class BedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ROOM = 0;
    private static final int TYPE_BED = 1;

    private Context context;
    private List<Item> items;
    private Staff me;
    private BedListener listener;

    public BedAdapter(Context context, List<Item> items, BedListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;

        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            me = null;
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ROOM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_header, parent, false);
            return new RoomViewHolder(view);
        } else if (viewType == TYPE_BED) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_bed, parent, false);
            return new BedViewHolder(view);
        }

        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (items.get(position) instanceof Room) {
            Room room = (Room) items.get(position);
            RoomViewHolder viewHolder = (RoomViewHolder) holder;

            String gender = "";
            if (room.getGender().equals("1")) {
                gender = "♂ ";
            } else if (room.getGender().equals("2")) {
                gender = "♀ ";
            } else {
                gender = "⚥ ";
            }

            viewHolder.txtName.setText(gender + room.getNumber() + " - " + room.getName() + " - " + room.getGenderName());
        } else if (items.get(position) instanceof Bed) {
            Bed bed = (Bed) items.get(position);
            BedViewHolder viewHolder = (BedViewHolder) holder;

            viewHolder.txtName.setText(bed.getName());
            viewHolder.btnAdd.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onAddClick(position);
                }
            });
            viewHolder.btnCheckout.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onCheckoutClick(position);
                }
            });

            if (bed.getBedPatientID() > 0) {
                viewHolder.txtCount.setText(String.valueOf(bed.getCount()));
                viewHolder.txtCount.setVisibility(View.VISIBLE);

                viewHolder.image.setImageResource(R.drawable.ic_account);

                viewHolder.txtPatient.setText(bed.getPatientName());

                viewHolder.btnAdd.setVisibility(View.GONE);
                viewHolder.btnCheckout.setVisibility(View.GONE);


                if ((me.getRoleID().equals("2") && me.getStaffID() == bed.getStaffID() ) || me.getRoleID().equals("1")) {
                    viewHolder.btnCheckout.setVisibility(View.VISIBLE);
                }


                viewHolder.parent.setCardBackgroundColor(Color.parseColor("#ffebee"));

                viewHolder.itemView.setOnClickListener(view -> {
                    if (listener != null) {
                        listener.onItemClick(bed);
                    }
                });
            } else {
                viewHolder.txtCount.setVisibility(View.GONE);

                viewHolder.image.setImageResource(R.drawable.ic_check);

                viewHolder.txtPatient.setText(R.string.bed_empty);

                viewHolder.btnAdd.setVisibility(View.VISIBLE);
                viewHolder.btnCheckout.setVisibility(View.GONE);
                if (me.getRoleID().equals("1")) {
                    viewHolder.btnAdd.setVisibility(View.VISIBLE);
                }
//                 y ta
//                if (me.getRoleID().equals("2")) {
//                    viewHolder.btnAdd.setVisibility(View.GONE);
//                }







                viewHolder.parent.setCardBackgroundColor(Color.parseColor("#e8f5e9"));      }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof Room) {
            return TYPE_ROOM;
        }

        return TYPE_BED;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName;

        public RoomViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
        }
    }

    public class BedViewHolder extends RecyclerView.ViewHolder {

        private CardView parent;
        private TextView txtName, txtCount, txtPatient;
        private ImageView image;
        private Button btnAdd, btnCheckout;

        public BedViewHolder(@NonNull View itemView) {
            super(itemView);

            parent = itemView.findViewById(R.id.parent);
            txtName = itemView.findViewById(R.id.txt_name);
            txtCount = itemView.findViewById(R.id.txt_cout);
            txtPatient = itemView.findViewById(R.id.txt_patient);
            image = itemView.findViewById(R.id.image);
            btnAdd = itemView.findViewById(R.id.btn_add);
            btnCheckout = itemView.findViewById(R.id.btn_checkout);
        }
    }

    public interface BedListener {

        void onAddClick(int position);

        void onCheckoutClick(int position);

        void onItemClick(Bed bed);
    }
}
