package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.Patient;

import java.util.List;

public class PatientArrayAdapter extends ArrayAdapter<Patient> {

    private final Context context;
    private final List<Patient> patients;
    private final int layoutResourceID;

    public PatientArrayAdapter(@NonNull Context context, int resource, List<Patient> patients) {
        super(context, resource);

        this.context = context;
        this.layoutResourceID = resource;
        this.patients = patients;
    }

    public int getCount() {
        return patients.size();
    }

    public Patient getItem(int position) {
        return patients.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceID, parent, false);
            }

            Patient patient = getItem(position);
            TextView name = (TextView) convertView.findViewById(R.id.textView);
            name.setText(patient.getId() + " - " + patient.getFullname());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
}
