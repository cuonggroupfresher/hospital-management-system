package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.Patient;
import com.android.hospitalapp.model.Person;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.util.StringUtils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> implements Filterable {

    private static final String TAG = PersonAdapter.class.getName();

    private Context context;
    private List<Person> personList;
    private List<Person> filteredList;
    private PersonListener listener;

    public PersonAdapter(Context context, List<Person> personList, PersonListener listener) {
        this.context = context;
        this.personList = personList;
        this.filteredList = personList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Person person = filteredList.get(position);

        holder.txtID.setText(person.getId());
        holder.txtName.setText(person.getFullname());
        if (person.getGender().equals("1")) {
            holder.txtGender.setText(context.getString(R.string.male));
        } else if (person.getGender().equals("2")) {
            holder.txtGender.setText(context.getString(R.string.female));
        } else {
            holder.txtGender.setText(context.getString(R.string.other));
        }
        holder.txtBirthday.setText(person.getBirthday());

        if (person instanceof Patient) {
            holder.image.setImageResource(R.drawable.ic_patient);
        } else if (person instanceof Staff) {
            Staff staff = (Staff) person;
            if (staff.getRoleID().equals("2")) {
                holder.image.setImageResource((staff.getGender().equals("2") ? R.drawable.ic_doctor_f : R.drawable.ic_doctor));
            } else {
                holder.image.setImageResource(R.drawable.ic_nurses);
            }
        }

        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClick(person);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d(TAG, "performFiltering: " + charSequence);

                String query = StringUtils.removeAccent(charSequence.toString()).toLowerCase();
                if (query.isEmpty()) {
                    filteredList = personList;
                } else {
                    List<Person> list = new ArrayList<>();
                    for (Person person : personList) {
                        if (StringUtils.removeAccent(person.getFullname()).toLowerCase().contains(query)) {
                            list.add(person);
                        }
                    }
                    filteredList = list;
                }

                FilterResults results = new FilterResults();
                results.values = filteredList;
                return results;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<Person>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void updateFiltered() {
        filteredList = personList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image;
        private TextView txtID, txtName, txtBirthday, txtGender;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            txtID = itemView.findViewById(R.id.txt_id);
            txtName = itemView.findViewById(R.id.txt_name);
            txtGender = itemView.findViewById(R.id.txt_gender);
            txtBirthday = itemView.findViewById(R.id.txt_birthday);
        }
    }

    public interface PersonListener {

        void onItemClick(Person person);
    }
}
