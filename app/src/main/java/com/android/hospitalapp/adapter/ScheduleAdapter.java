package com.android.hospitalapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.model.Schedule;

import java.util.ArrayList;
import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    private Context context;
    private List<Schedule> schedules;
    private ScheduleListener listener;

    public ScheduleAdapter(Context context, List<Schedule> schedules, ScheduleListener listener) {
        this.context = context;
        this.schedules = new ArrayList<>(schedules);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_schedule, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Schedule schedule = schedules.get(position);

        String title = "";
        // if (schedule.getRoleID().equals("2")) {
        //     title = "Bác sĩ ";
        // } else if (schedule.getRoleID().equals("3")) {
        //     title = "Y tá ";
        // }

        holder.txtTitle.setText(schedule.getUserID() + " - " + title + schedule.getName() + " - " + schedule.getPhoneNumber());
        if (schedule.getDescription() == null || schedule.getDescription().isEmpty()) {
            holder.txtDetail.setVisibility(View.GONE);
        } else {
            holder.txtDetail.setVisibility(View.GONE);
            holder.txtDetail.setText("- " + schedule.getDescription());
        }

        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClick(schedule);
            }
        });
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setSchedules(List<Schedule> schedules) {
        this.schedules = new ArrayList<>(schedules);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle, txtDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.txt_title);
            txtDetail = itemView.findViewById(R.id.txt_detail);
        }
    }

    public interface ScheduleListener {

        void onItemClick(Schedule schedule);
    }
}
