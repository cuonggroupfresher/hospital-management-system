package com.android.hospitalapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Schedule implements Item, Serializable {

    @SerializedName("schedule_id")
    private int id;
    @SerializedName("schedule_date")
    private String date;
    @SerializedName("schedule_description")
    private String description;
    @SerializedName("id_user")
    private String userID;
    @SerializedName("fullname")
    private String name;
    @SerializedName("number_phone")
    private String phoneNumber;
    @SerializedName("image")
    private String image;
    @SerializedName("is_staff")
    private boolean isStaff;
    @SerializedName("otp")
    private String otp;
    @SerializedName("address")
    private String address;
    @SerializedName("detail_address")
    private String detailAddress;
    @SerializedName("gender")
    private String gender;
    @SerializedName("gender_name")
    private String genderName;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("wards_code")
    private int wardsCode;
    @SerializedName("city_code")
    private int cityCode;
    @SerializedName("districts_code")
    private int districtsCode;
    @SerializedName("wards_name")
    private String wardsName;
    @SerializedName("city_name")
    private String cityName;
    @SerializedName("districts_name")
    private String districtsName;
    @SerializedName("staff_id")
    private int staffID;
    @SerializedName("role_id")
    private String roleID;
    @SerializedName("role_name")
    private String roleName;
    @SerializedName("position")
    private String position;
    @SerializedName("introduction")
    private String introduction;

    public Schedule() {}

    public Schedule(int id, String date, String description, String userID, String name, String phoneNumber, String image, boolean isStaff, String otp, String address, String detailAddress, String gender, String genderName, String birthday, int wardsCode, int cityCode, int districtsCode, String wardsName, String cityName, String districtsName, int staffID, String roleID, String roleName, String position, String introduction) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.userID = userID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.isStaff = isStaff;
        this.otp = otp;
        this.address = address;
        this.detailAddress = detailAddress;
        this.gender = gender;
        this.genderName = genderName;
        this.birthday = birthday;
        this.wardsCode = wardsCode;
        this.cityCode = cityCode;
        this.districtsCode = districtsCode;
        this.wardsName = wardsName;
        this.cityName = cityName;
        this.districtsName = districtsName;
        this.staffID = staffID;
        this.roleID = roleID;
        this.roleName = roleName;
        this.position = position;
        this.introduction = introduction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isStaff() {
        return isStaff;
    }

    public void setStaff(boolean staff) {
        isStaff = staff;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getWardsCode() {
        return wardsCode;
    }

    public void setWardsCode(int wardsCode) {
        this.wardsCode = wardsCode;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public int getDistrictsCode() {
        return districtsCode;
    }

    public void setDistrictsCode(int districtsCode) {
        this.districtsCode = districtsCode;
    }

    public String getWardsName() {
        return wardsName;
    }

    public void setWardsName(String wardsName) {
        this.wardsName = wardsName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictsName() {
        return districtsName;
    }

    public void setDistrictsName(String districtsName) {
        this.districtsName = districtsName;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", description='" + description + '\'' +
                ", userID='" + userID + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", image='" + image + '\'' +
                ", isStaff=" + isStaff +
                ", otp='" + otp + '\'' +
                ", address='" + address + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                ", gender='" + gender + '\'' +
                ", genderName='" + genderName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", wardsCode=" + wardsCode +
                ", cityCode=" + cityCode +
                ", districtsCode=" + districtsCode +
                ", wardsName='" + wardsName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", districtsName='" + districtsName + '\'' +
                ", staffID=" + staffID +
                ", roleID='" + roleID + '\'' +
                ", roleName='" + roleName + '\'' +
                ", position='" + position + '\'' +
                ", introduction='" + introduction + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return id == schedule.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
