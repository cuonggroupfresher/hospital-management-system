package com.android.hospitalapp.model;

import java.io.Serializable;
import java.util.Objects;

public class DateHeader implements Item, Serializable {

    private String date;

    public DateHeader() {}

    public DateHeader(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "DateHeader{" +
                "date='" + date + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateHeader header = (DateHeader) o;
        return Objects.equals(date, header.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }
}
