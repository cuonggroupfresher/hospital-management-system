package com.android.hospitalapp.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NotificationModel {

    private final Calendar time;
    private final String content;

    public NotificationModel(Calendar time, String content) {
        this.time = time;
        this.content = content;
    }

    public Calendar getTime() {
        return time;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        return "NotificationModel{" +
                "time='" + dateFormat.format(time.getTime()) +
                "', content='" + content + '\'' +
                '}';
    }
}
