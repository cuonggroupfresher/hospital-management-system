package com.android.hospitalapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Staff extends Person implements Serializable {

    @SerializedName("staff_id")
    protected int staffID;
    @SerializedName("role_id")
    protected String roleID;
    @SerializedName("role_name")
    protected String roleName;
    protected String position;
    protected String introduction;

    public Staff() {}

    public Staff(String id, String fullname, String phoneNumber, String image, String gender, String genderName, String birthday, String address, String otp, boolean isStaff, int staffID, String roleID, String roleName, String position, String introduction) {
        super(id, fullname, phoneNumber, image, gender, genderName, birthday, address, otp, isStaff);

        this.staffID = staffID;
        this.roleID = roleID;
        this.roleName = roleName;
        this.position = position;
        this.introduction = introduction;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Override
    public String toString() {
        return id + " - " + fullname;
    }

    public String getString() {
        return "Staff{" +
                "id='" + id + '\'' +
                ", fullname='" + fullname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", image='" + image + '\'' +
                ", gender='" + gender + '\'' +
                ", genderName='" + genderName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", address='" + address + '\'' +
                ", otp='" + otp + '\'' +
                ", isStaff=" + isStaff +
                ", staffID=" + staffID +
                ", roleID='" + roleID + '\'' +
                ", roleName='" + roleName + '\'' +
                ", position='" + position + '\'' +
                ", introduction='" + introduction + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Staff staff = (Staff) o;
        return staffID == staff.staffID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(staffID);
    }
}
