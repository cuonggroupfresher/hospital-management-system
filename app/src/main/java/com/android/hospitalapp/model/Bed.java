package com.android.hospitalapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Bed implements Item, Serializable {

    @SerializedName("bed")
    private String id;
    @SerializedName("bed_name")
    private String name;
    private String description;
    @SerializedName("bed_patient_id")
    private int bedPatientID;
    @SerializedName("staff_id")
    private int staffID;
    @SerializedName("staff_fullname")
    private String staffName;
    @SerializedName("staff_number_phone")
    private String staffPhone;
    @SerializedName("staff_id_user")
    private String staffIDUser;
    @SerializedName("patient_id")
    private int patientID;
    @SerializedName("patient_fullname")
    private String patientName;
    @SerializedName("patient_number_phone")
    private String patientPhone;
    @SerializedName("patient_id_user")
    private String patientIDUser;
    @SerializedName("date_in")
    private String dateIn;
    @SerializedName("count_date")
    private int count;
    @SerializedName("room_number")
    private String roomNumber;
    @SerializedName("gender_display")
    private String gender;
    private int roomID;
    private String room;
    private String roomType;

    public Bed() {}

    public Bed(String id, String name, String description, int bedPatientID, int staffID, String staffName, String staffPhone, String staffIDUser, int patientID, String patientName, String patientPhone, String patientIDUser, String dateIn, int count, String roomNumber, String gender) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.bedPatientID = bedPatientID;
        this.staffID = staffID;
        this.staffName = staffName;
        this.staffPhone = staffPhone;
        this.staffIDUser = staffIDUser;
        this.patientID = patientID;
        this.patientName = patientName;
        this.patientPhone = patientPhone;
        this.patientIDUser = patientIDUser;
        this.dateIn = dateIn;
        this.count = count;
        this.roomNumber = roomNumber;
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBedPatientID() {
        return bedPatientID;
    }

    public void setBedPatientID(int bedPatientID) {
        this.bedPatientID = bedPatientID;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getStaffIDUser() {
        return staffIDUser;
    }

    public void setStaffIDUser(String staffIDUser) {
        this.staffIDUser = staffIDUser;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getPatientIDUser() {
        return patientIDUser;
    }

    public void setPatientIDUser(String patientIDUser) {
        this.patientIDUser = patientIDUser;
    }

    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void checkOut() {
        bedPatientID = 0;
        count = 0;
        dateIn = "";
        roomNumber = "";

        staffID = 0;
        staffName = "";
        staffPhone = "";
        staffIDUser = "";

        patientID = 0;
        patientName = "";
        patientPhone = "";
        patientIDUser = "";
    }

    @Override
    public String toString() {
        return "Bed{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", bedPatientID=" + bedPatientID +
                ", staffID=" + staffID +
                ", staffName='" + staffName + '\'' +
                ", staffPhone='" + staffPhone + '\'' +
                ", staffIDUser='" + staffIDUser + '\'' +
                ", patientID=" + patientID +
                ", patientName='" + patientName + '\'' +
                ", patientPhone='" + patientPhone + '\'' +
                ", patientIDUser='" + patientIDUser + '\'' +
                ", dateIn='" + dateIn + '\'' +
                ", count=" + count +
                ", roomNumber=" + roomNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bed bed = (Bed) o;
        return roomID == bed.roomID && Objects.equals(id, bed.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomID);
    }
}
