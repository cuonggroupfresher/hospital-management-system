package com.android.hospitalapp.model;

import com.google.gson.JsonObject;

public class Response {

    private String status;
    private int code;
    private String message;
    private JsonObject results;

    public Response() {}

    public Response(String status, int code, String message, JsonObject results) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonObject getResults() {
        return results;
    }

    public void setResults(JsonObject results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Response{" +
                "status='" + status + '\'' +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", results=" + results +
                '}';
    }
}
