package com.android.hospitalapp.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Room implements Item, Serializable {

    @SerializedName("room_id")
    private int id;
    @SerializedName("room_number")
    private String number;
    @SerializedName("room")
    private String name;
    @SerializedName("type")
    private String type;
    @SerializedName("type_display")
    private String typeName;
    private String gender;
    @SerializedName("gender_display")
    private String genderName;
    @SerializedName("list_bed")
    private JsonArray bedList;

    public Room() {}

    public Room(int id, String number, String name, String type, String typeName, String gender, String genderName, JsonArray bedList) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.type = type;
        this.typeName = typeName;
        this.gender = gender;
        this.genderName = genderName;
        this.bedList = bedList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public JsonArray getBedList() {
        return bedList;
    }

    public void setBedList(JsonArray bedList) {
        this.bedList = bedList;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", typeName='" + typeName + '\'' +
                ", bedList=" + bedList +
                '}';
    }
}
