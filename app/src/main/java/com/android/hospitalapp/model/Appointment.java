package com.android.hospitalapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Appointment implements Item, Serializable {

    private int id;
    private int patient;
    private int staff;
    private String date;
    private String time;
    @SerializedName("is_rexamination")
    private boolean isReExamination;
    private String description;
    private boolean status;
    @SerializedName("time_display")
    private String timeDisplay;
    @SerializedName("rexamination")
    private String reExamination;
    @SerializedName("patient_fullname")
    private String patientName;
    @SerializedName("patient_number_phone")
    private String patientPhone;
    @SerializedName("patient_address")
    private String patientAddress;
    @SerializedName("patient_id_user")
    private String patientIDUser;
    @SerializedName("staff_id_user")
    private String staffIDUser;
    @SerializedName("staff_fullname")
    private String staffName;
    @SerializedName("staff_position")
    private String staffPosition;
    @SerializedName("staff_number_phone")
    private String staffPhone;

    public Appointment() {}

    public Appointment(int id, int patient, int staff, String date, String time, boolean isReExamination, String description, boolean status, String timeDisplay, String reExamination, String patientName, String patientPhone, String patientAddress, String patientIDUser, String staffIDUser, String staffName, String staffPosition, String staffPhone) {
        this.id = id;
        this.patient = patient;
        this.staff = staff;
        this.date = date;
        this.time = time;
        this.isReExamination = isReExamination;
        this.description = description;
        this.status = status;
        this.timeDisplay = timeDisplay;
        this.reExamination = reExamination;
        this.patientName = patientName;
        this.patientPhone = patientPhone;
        this.patientAddress = patientAddress;
        this.patientIDUser = patientIDUser;
        this.staffIDUser = staffIDUser;
        this.staffName = staffName;
        this.staffPosition = staffPosition;
        this.staffPhone = staffPhone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatient() {
        return patient;
    }

    public void setPatient(int patient) {
        this.patient = patient;
    }

    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isReExamination() {
        return isReExamination;
    }

    public void setReExamination(boolean reExamination) {
        isReExamination = reExamination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTimeDisplay() {
        return timeDisplay;
    }

    public void setTimeDisplay(String timeDisplay) {
        this.timeDisplay = timeDisplay;
    }

    public String getReExamination() {
        return reExamination;
    }

    public void setReExamination(String reExamination) {
        this.reExamination = reExamination;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientIDUser() {
        return patientIDUser;
    }

    public void setPatientIDUser(String patientIDUser) {
        this.patientIDUser = patientIDUser;
    }

    public String getStaffIDUser() {
        return staffIDUser;
    }

    public void setStaffIDUser(String staffIDUser) {
        this.staffIDUser = staffIDUser;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffPosition() {
        return staffPosition;
    }

    public void setStaffPosition(String staffPosition) {
        this.staffPosition = staffPosition;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appointment that = (Appointment) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", patient=" + patient +
                ", staff=" + staff +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", isReExamination=" + isReExamination +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", timeDisplay='" + timeDisplay + '\'' +
                ", reExamination='" + reExamination + '\'' +
                ", patientName='" + patientName + '\'' +
                ", patientPhone='" + patientPhone + '\'' +
                ", patientAddress='" + patientAddress + '\'' +
                ", patientIDUser='" + patientIDUser + '\'' +
                ", staffIDUser='" + staffIDUser + '\'' +
                ", staffName='" + staffName + '\'' +
                ", staffPosition='" + staffPosition + '\'' +
                ", staffPhone='" + staffPhone + '\'' +
                '}';
    }
}
