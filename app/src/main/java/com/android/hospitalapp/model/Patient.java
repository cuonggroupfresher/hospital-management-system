package com.android.hospitalapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Patient extends Person implements Serializable {

    @SerializedName("patient_id")
    protected int patientID;

    public Patient() {}

    public Patient(String id, String fullname, String phoneNumber, String image, String gender, String genderName, String birthday, String address, String otp, boolean isStaff, int patientID) {
        super(id, fullname, phoneNumber, image, gender, genderName, birthday, address, otp, isStaff);

        this.patientID = patientID;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    @Override
    public String toString() {
        return id + " - " + fullname;
    }

    public String getString() {
        return "Patient{" +
                "patientID=" + patientID +
                ", id='" + id + '\'' +
                ", fullname='" + fullname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", image='" + image + '\'' +
                ", gender='" + gender + '\'' +
                ", genderName='" + genderName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", address='" + address + '\'' +
                ", otp='" + otp + '\'' +
                ", isStaff=" + isStaff +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return patientID == patient.patientID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientID);
    }
}
