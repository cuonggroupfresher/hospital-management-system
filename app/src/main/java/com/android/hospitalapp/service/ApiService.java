package com.android.hospitalapp.service;

import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiService {

    // String BASE_URL = "http://103.216.113.130:8008/";
    String BASE_URL = "http://103.216.113.130:8088/";


    ApiService apiService = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(new OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService.class);

    @POST("api/v1/login/")
    Call<JsonObject> login(@Body JsonObject data);

    @POST("api/v1/staff/create_staff/")
    Call<JsonObject> createStaff(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/staff/get_list_staff_by_role/")
    Call<JsonObject> getStaffs(@Header("Authorization") String token, @Body JsonObject data);

    @GET("api/v1/staff/{id}")
    Call<JsonObject> getStaff(@Path("id") int id, @Header("Authorization") String token);

    @PUT("api/v1/staff/{id}/")
    Call<JsonObject> updateStaff(@Path("id") int id, @Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/patient/create_patient/")
    Call<JsonObject> createPatient(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/patient/list_all_patient/")
    Call<JsonObject> getPatients(@Header("Authorization") String token, @Body JsonObject data);

    @GET("api/v1/patient/{id}")
    Call<JsonObject> getPatient(@Path("id") int id, @Header("Authorization") String token);

    @PUT("api/v1/patient/{id}/")
    Call<JsonObject> updatePatient(@Path("id") int id, @Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/bed/list_all_bed_patient/")
    Call<JsonObject> getRooms(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/bed/add_patient_to_bed/")
    Call<JsonObject> insertToBed(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/bed/checkout/")
    Call<JsonObject> checkoutBed(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/schedule/list_all_schedule_all_staff_by_month_and_year/")
    Call<JsonObject> getSchedules(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/schedule/update_schedule/")
    Call<JsonObject> updateSchedule(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/appointment/check_appointment_patient_next_day/")
    Call<JsonObject> getTomorrowAppointment(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/appointment/get_all_appointment_feature/")
    Call<JsonObject> getFutureAppointments(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/appointment/list_appointment_staff_by_week/")
    Call<JsonObject> getAppointments(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/appointment/update_appointment/")
    Call<JsonObject> updateAppointment(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/appointment/delete_appointment/")
    Call<JsonObject> removeAppointment(@Header("Authorization") String token, @Body JsonObject data);

    @POST("api/v1/schedule/list_all_schedule_staff_feature/")
    Call<JsonObject> getFutureSchedule(@Header("Authorization") String token, @Body JsonObject data);
}
