package com.android.hospitalapp.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.android.hospitalapp.R;

import java.util.Calendar;

public class MonthYearPickerDialog extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;

    private NumberPicker monthPicker;
    private NumberPicker yearPicker;

    private final Calendar cal = Calendar.getInstance();

    public static final String MONTH_KEY = "month_value";
    public static final String YEAR_KEY = "year_value";

    private int monthVal = -1, yearVal = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getArguments();
        if (extras != null) {
            monthVal = extras.getInt(MONTH_KEY, -1);
            yearVal = extras.getInt(YEAR_KEY, -1);
        }
    }

    public static MonthYearPickerDialog newInstance(int monthIndex, int yearIndex) {
        MonthYearPickerDialog f = new MonthYearPickerDialog();

        Bundle args = new Bundle();
        args.putInt(MONTH_KEY, monthIndex);
        args.putInt(YEAR_KEY, yearIndex);
        f.setArguments(args);

        return f;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog = inflater.inflate(R.layout.dialog_month_year_picker, null);
        monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
        yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);

        if (monthVal != -1) {
            monthPicker.setValue(monthVal);
        } else {
            monthPicker.setValue(cal.get(Calendar.MONTH) + 1);
        }

        monthPicker.setDisplayedValues(new String[]{"Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"});

        int maxYear = 2099;
        final int minYear = 1900;
        int arraySize = maxYear - minYear;

        String[] tempArray = new String[arraySize];
        tempArray[0] = "---";
        int tempYear = minYear + 1;

        for (int i = 0; i < arraySize; i++) {
            if (i != 0) {
                tempArray[i] = " " + tempYear + "";
            }
            tempYear++;
        }
        Log.i("", "onCreateDialog: " + tempArray.length);
        yearPicker.setMinValue(minYear + 1);
        yearPicker.setMaxValue(maxYear);
        yearPicker.setDisplayedValues(tempArray);

        if (yearVal != -1) {
            yearPicker.setValue(yearVal);
        } else {
            yearPicker.setValue(tempYear - 1);
        }

        builder.setView(dialog)
                .setPositiveButton(R.string.ok, (dialog12, id) -> {
                    int year = yearPicker.getValue();
                    if (year == (minYear + 1)) {
                        year = 1900;
                    }

                    listener.onDateSet(null, year, monthPicker.getValue() - 1, -1);
                })
                .setNegativeButton(R.string.cancel, (dialog1, id) -> MonthYearPickerDialog.this.getDialog().cancel());

        return builder.create();
    }
}
