package com.android.hospitalapp.constant;

public final class Constants {

    public static final String SHARED_PREFERENCES_NAME = "hospital";

    public static final String PREF_PHONE_NUMBER = "phone_number";
    public static final String PREF_PASSWORD = "password";
    public static final String PREF_ACCESS_TOKEN = "access_token";
    public static final String PREF_REFRESH_TOKEN = "refresh_token";
    public static final String PREF_INFO = "info";
    public static final String PREF_NUM_ALERT = "num_alert";
}
