package com.android.hospitalapp.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.NotificationModel;
import com.android.hospitalapp.service.SchedulingService;

import java.util.Calendar;
import java.util.List;

public class AlarmUtils {

    public static void clean(Context context) {
        if (context == null) {
            return;
        }

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SchedulingService.class);

        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        int n = sharedPreferences.getInt(Constants.PREF_NUM_ALERT, 0);

        for (int i = 1; i <= n; i++) {
            PendingIntent pendingIntent = PendingIntent.getService(context, i, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pendingIntent);
        }
    }

    public static void create(Context context, NotificationModel... notificationModels) {
        Log.d("AlarmUtils", "create: ");

        if (context == null || notificationModels == null) {
            return;
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Constants.PREF_NUM_ALERT, notificationModels.length);
        editor.apply();

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int code = 1;
        for (NotificationModel model : notificationModels) {
            Intent intent = new Intent(context, SchedulingService.class);
            intent.putExtra("content", model.getContent());
            PendingIntent pendingIntent = PendingIntent.getService(context, code++, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, model.getTime().getTimeInMillis(), pendingIntent);
        }
    }

    public static void create(Context context, List<NotificationModel> notificationModels) {
        Log.d("AlarmUtils", "create: ");

        if (context == null || notificationModels == null) {
            return;
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Constants.PREF_NUM_ALERT, notificationModels.size());
        editor.apply();

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int code = 1;
        for (NotificationModel model : notificationModels) {
            Log.d("AlarmUtils", "create: " + model);
            Intent intent = new Intent(context, SchedulingService.class);
            intent.putExtra("content", model.getContent());
            PendingIntent pendingIntent = PendingIntent.getService(context, code++, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, model.getTime().getTimeInMillis(), pendingIntent);
        }
    }
}
