package com.android.hospitalapp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.activity.LoginActivity;
import com.android.hospitalapp.activity.PatientActivity;
import com.android.hospitalapp.activity.StaffActivity;
import com.android.hospitalapp.adapter.PatientBedAdapter;
import com.android.hospitalapp.adapter.PersonAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Appointment;
import com.android.hospitalapp.model.Bed;
import com.android.hospitalapp.model.Patient;
import com.android.hospitalapp.model.Person;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManagerFragment extends Fragment {

    private static final String TAG = ManagerFragment.class.getName();

    private Spinner spinner;
    private RecyclerView recyclerView;
    private RecyclerView recyclerViewBed;
    private PersonAdapter adapter;
    private PatientBedAdapter patientBedAdapter;
    private FloatingActionButton btnAdd;
    private EditText edtSearch;

    private ActivityResultLauncher<Intent> resultLauncher;

    private List<Person> people;
    private List<Bed> beds;
    private Staff staff, me;
    private int current = -1;

    public ManagerFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {}

        try {
            SharedPreferences sharedPreferences = getContext().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (me == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: " + me);

        resultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onActivityResult);

        people = new ArrayList<>();
        beds = new ArrayList<>();

        Log.d(TAG, "onCreate: Created");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Created");

        View view = inflater.inflate(R.layout.fragment_manager, container, false);

        view.findViewById(R.id.btn_logout).setOnClickListener(v -> logout());

        ArrayAdapter<String> arrayAdapter;
        if (me.getRoleID().equals("1")) {
            arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, getResources().getStringArray(R.array.manager_list_1));
        } else if (me.getRoleID().equals("2")) {
            arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, getResources().getStringArray(R.array.manager_list_2));
        } else {
            arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, getResources().getStringArray(R.array.manager_list_3));
        }

        arrayAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        spinner = view.findViewById(R.id.spinner);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                if (current != pos) {
                    if (spinner.getSelectedItem().equals(getString(R.string.manager_patient))) {
                        recyclerViewBed.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        edtSearch.setVisibility(View.GONE);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerViewBed.setVisibility(View.GONE);
                        edtSearch.setVisibility(View.VISIBLE);
                    }

                    current = pos;

                    Log.d(TAG, "onItemSelected: changed " + pos + ", " + id + ", " + spinner.getSelectedItem());

                    if (staff.getRoleID().equals("1") || spinner.getSelectedItem().equals(getString(R.string.patient))) {
                        btnAdd.setVisibility(View.VISIBLE);
                        btnAdd.setVisibility(View.GONE);
                    } else {
                        btnAdd.setVisibility(View.GONE);
                    }

                    ManagerFragment.this.people.clear();
                    adapter.notifyDataSetChanged();

                    edtSearch.setText("");

                    getData(spinner.getSelectedItem().toString());
                } else {
                    Log.d(TAG, "onItemSelected: not changed " + pos + ", " + id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        adapter = new PersonAdapter(getActivity(), people, this::onItemClick);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        patientBedAdapter = new PatientBedAdapter(getActivity(), beds, null);

        recyclerViewBed = view.findViewById(R.id.recycler_view_2);
        recyclerViewBed.setAdapter(patientBedAdapter);
        recyclerViewBed.setLayoutManager(new LinearLayoutManager(getActivity()));

        btnAdd = view.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this::onAddClicked);
        if (getActivity() != null) {
            try {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                String info = sharedPreferences.getString(Constants.PREF_INFO, "");
                staff = new Gson().fromJson(info, Staff.class);
                if (!staff.getRoleID().equals("1")) {
                    btnAdd.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        edtSearch = view.findViewById(R.id.edt_search);
        edtSearch.clearFocus();
        edtSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                adapter.getFilter().filter(edtSearch.getText().toString().trim());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return true;
            }
            return false;
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        return view;
    }

    private void onItemClick(Person person) {
        Log.d(TAG, "onItemClick: " + person);

        if (person instanceof Staff) {
            Staff staff = (Staff) person;
            Intent intent = new Intent(getActivity(), StaffActivity.class);
            intent.putExtra("mode", 0);
            intent.putExtra("staff_id", staff.getStaffID());
            resultLauncher.launch(intent);
        } else if (person instanceof Patient) {
            Patient patient = (Patient) person;
            Intent intent = new Intent(getActivity(), PatientActivity.class);
            intent.putExtra("mode", 1);
            intent.putExtra("patient_id", patient.getPatientID());
            resultLauncher.launch(intent);
        }
    }

    private void logout() {
        if (getActivity() != null ) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Đăng xuất");
            alert.setMessage("Bạn có chắc muốn đăng xuất?");
            alert.setPositiveButton(android.R.string.yes, (dialog, which) ->  {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            });
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        }
    }

    private void onAddClicked(View view) {
        if (spinner.getSelectedItem().equals(getString(R.string.doctor))) {
            Intent intent = new Intent(getActivity(), StaffActivity.class);
            intent.putExtra("mode", 1);
            resultLauncher.launch(intent);
        } else if (spinner.getSelectedItem().equals(getString(R.string.nurse))) {
            Intent intent = new Intent(getActivity(), StaffActivity.class);
            intent.putExtra("mode", 2);
            resultLauncher.launch(intent);
        } else if (spinner.getSelectedItem().equals(getString(R.string.patient))) {
            Intent intent = new Intent(getActivity(), PatientActivity.class);
            intent.putExtra("mode", 2);
            resultLauncher.launch(intent);
        }
    }

    private void onActivityResult(ActivityResult result) {
        Log.d(TAG, "onActivityResult: " + result);

        if (result != null && result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            Log.d(TAG, "onActivityResult: " + result.getData());

            if (spinner.getSelectedItem().equals(getString(R.string.doctor)) || spinner.getSelectedItem().equals(getString(R.string.nurse))) {
                Staff staff = (Staff) result.getData().getSerializableExtra("data");
                if (staff == null) {
                    return;
                }

                int index = people.indexOf(staff);
                if (index != -1) {
                    people.set(index, staff);
                } else {
                    people.add(staff);
                }

                Log.d(TAG, "onActivityResult " + staff);
            } else if (spinner.getSelectedItem().equals(getString(R.string.patient))) {
                Patient patient = (Patient) result.getData().getSerializableExtra("data");
                if (patient == null) {
                    return;
                }

                int index = people.indexOf(patient);
                if (index != -1) {
                    people.set(index, patient);
                } else {
                    people.add(patient);
                }
                Log.d(TAG, "onActivityResult " + patient);
            }

            adapter.notifyDataSetChanged();
        }
    }

    private void getData(String type) {
        if (type == null || getActivity() == null) {
            return;
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }

        Log.d(TAG, "getData: token = " + token);

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();

        if (type.equals(getString(R.string.doctor))) {
            jsonObject.add("role_id", gson.toJsonTree(2));

            ApiService.apiService.getStaffs(token, jsonObject).enqueue(callback);
        } else if (type.equals(getString(R.string.nurse))) {
            jsonObject.add("role_id", gson.toJsonTree(3));

            ApiService.apiService.getStaffs(token, jsonObject).enqueue(callback);
        } else if (type.equals(getString(R.string.patient))) {
            ApiService.apiService.getPatients(token, jsonObject).enqueue(callback);
        } else if (type.equals(getString(R.string.manager_patient))) {
            ApiService.apiService.getStaff(me.getStaffID(), token).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (spinner == null || getActivity() == null || response.body() == null) {
                        Log.d(TAG, "onResponse: " + response);

                        Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Log.d(TAG, "onResponse: " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        Toast.makeText(getContext(), response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Gson gson = new Gson();
                    JsonObject object = response.body().get("results").getAsJsonObject();

                    Type listType = new TypeToken<List<Bed>>() {}.getType();
                    List<Bed> beds = gson.fromJson(object.get("list_patient_in_bed"), listType);
                    Log.d(TAG, "onResponse: " + beds);
                    ManagerFragment.this.beds.clear();
                    ManagerFragment.this.beds.addAll(beds);

                    patientBedAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void backToLogin() {
        if (getActivity() == null) {
            return;
        }

        Toast.makeText(getActivity(), R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    private final Callback<JsonObject> callback = new Callback<JsonObject>() {

        @Override
        public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
            if (response.code() == 401) {
                backToLogin();
                return;
            }

            if (spinner == null || getActivity() == null || response.body() == null) {
                Log.d(TAG, "onResponse: " + response);

                Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                return;
            }

            Log.d(TAG, "onResponse: " + response.body());

            if (spinner.getSelectedItem().toString().equals(getString(R.string.doctor)) || spinner.getSelectedItem().toString().equals(getString(R.string.nurse))) {
                Log.d(TAG, "onResponse: Staff");

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Staff>>() {}.getType();

                List<Person> list = new Gson().fromJson(json, listType);
                for (Person person : list) {
                    Log.d(TAG, "onResponse: " + person.getImage() + ", " + person);
                }
                if (people != null) {
                    people.addAll(list);
                }
            } else if (spinner.getSelectedItem().toString().equals(getString(R.string.patient))) {
                Log.d(TAG, "onResponse: Patients");

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Patient>>() {}.getType();

                List<Person> list = new Gson().fromJson(json, listType);
                for (Person person : list) {
                    Log.d(TAG, "onResponse: " + person.getImage() + ", " + person);
                }
                if (people != null) {
                    people.addAll(list);
                }
            } else {
                Log.d(TAG, "onResponse: Invalid selection");
            }

            adapter.updateFiltered();
            adapter.notifyDataSetChanged();
            recyclerView.smoothScrollToPosition(0);
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            t.printStackTrace();

            Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
        }
    };
}
