package com.android.hospitalapp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.dialog.MonthYearPickerDialog;
import com.android.hospitalapp.R;
import com.android.hospitalapp.activity.LoginActivity;
import com.android.hospitalapp.activity.ScheduleActivity;
import com.android.hospitalapp.adapter.ScheduleV2Adapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.DateHeader;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Schedule;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleV2Fragment extends Fragment {

    private static final String TAG = ScheduleV2Fragment.class.getName();

    private TextInputLayout edtMonth;
    private CheckBox checkBox;
    private RecyclerView recyclerView;
    private ScheduleV2Adapter adapter;

    private ActivityResultLauncher<Intent> resultLauncher;

    private List<Item> schedules;
    private List<Item> mySchedules;
    private Staff me;
    private DateHeader todayHeader;
    private String token;

    private ScheduleFragmentListener listener;

    public ScheduleV2Fragment(ScheduleFragmentListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {}

        if (getActivity() != null) {
            resultLauncher = getActivity().registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onActivityResult);
        }

        schedules = new ArrayList<>();
        mySchedules = new ArrayList<>();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        try {
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (me == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: Created");
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Created");

        View view = inflater.inflate(R.layout.fragment_schedule_v2, container, false);

        view.findViewById(R.id.btn_logout).setOnClickListener(v -> logout());

        adapter = new ScheduleV2Adapter(getActivity(), schedules, this::onItemClick);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        edtMonth = view.findViewById(R.id.edt_month);
        if (edtMonth.getEditText() != null) {
            edtMonth.getEditText().setOnClickListener(view1 -> {
                Calendar calendar = Calendar.getInstance();
                MonthYearPickerDialog pickerDialog = MonthYearPickerDialog.newInstance(calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
                pickerDialog.setListener((view2, year, month, day) -> {
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.set(Calendar.YEAR, year);
                    calendar1.set(Calendar.MONTH, month);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");
                    String monthStr = dateFormat.format(calendar1.getTime());
                    Log.d(TAG, "onYearMonthSet: (" + month + ", " + year + ") -> " + monthStr);

                    if (monthStr.equals(edtMonth.getEditText().getText().toString())) {
                        Log.d(TAG, "onYearMonthSet: not changed");
                    } else {
                        edtMonth.getEditText().setText(monthStr);
                        getSchedules(month + 1, year);
                    }
                });
                pickerDialog.show(getParentFragmentManager(), "MonthYearPickerDialog");
            });
        }

        checkBox = view.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                adapter.setItems(mySchedules);
                try {
                    if (todayHeader != null) {
                        if (checkBox.isChecked()) {
                            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(mySchedules.indexOf(todayHeader), 0);
                        } else {
                            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(schedules.indexOf(todayHeader), 0);
                        }
                    } else {
                        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(0, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                adapter.setItems(schedules);

                try {
                    if (todayHeader != null) {
                        if (checkBox.isChecked()) {
                            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(mySchedules.indexOf(todayHeader), 0);
                        } else {
                            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(schedules.indexOf(todayHeader), 0);
                        }
                    } else {
                        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(0, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (me.getRoleID().equals("1")) {
            checkBox.setVisibility(View.GONE);
        }

        view.findViewById(R.id.btn_switch).setOnClickListener(view12 -> {
            if (listener != null) {
                listener.onSwitchUI();
            }
        });

        Calendar today = Calendar.getInstance();
        int month = today.get(Calendar.MONTH) + 1;
        int year = today.get(Calendar.YEAR);
        edtMonth.getEditText().setText((month > 9 ? month : ("0" + month)) + "-" + year);
        getSchedules(month, year);

        return view;
    }

    private void onActivityResult(ActivityResult result) {
        Log.d(TAG, "onActivityResult: " + result);

        if (result != null && result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            Log.d(TAG, "onActivityResult: " + result.getData());

            Schedule schedule = (Schedule) result.getData().getSerializableExtra("data");
            if (schedule == null) {
                return;
            }

            Log.d(TAG, "onActivityResult: " + schedule);

            int index = schedules.indexOf(schedule);
            if (index != -1) {
                schedules.set(index, schedule);
            }

            index = mySchedules.indexOf(schedule);
            if (index != -1) {
                mySchedules.set(index, schedule);
            }

            if (getContext() != null) {
                Toast.makeText(getContext(), "Cập nhật lịch trực thành công", Toast.LENGTH_SHORT).show();
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void logout() {
        if (getActivity() != null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Đăng xuất");
            alert.setMessage("Bạn có chắc muốn đăng xuất?");
            alert.setPositiveButton(android.R.string.yes, (dialog, which) -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            });
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        }
    }

    private void onItemClick(Schedule schedule) {
        Log.d(TAG, "onItemClick: " + schedule);

        Intent intent = new Intent(getActivity(), ScheduleActivity.class);
        intent.putExtra("schedule", schedule);
        resultLauncher.launch(intent);
    }

    private void getSchedules(int month, int year) {
        Log.d(TAG, "getSchedules: " + month + "-" + year);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String today = format.format(calendar.getTime());

        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("month", gson.toJsonTree(month));
        body.add("year", gson.toJsonTree(year));

        ApiService.apiService.getSchedules(token, body).enqueue(new Callback<JsonObject>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (getActivity() == null || response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    if (getContext() != null) {
                        Toast.makeText(getContext(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    if (getContext() != null) {
                        Toast.makeText(getActivity(), response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                Gson gson = new Gson();
                JsonElement json = response.body().get("results").getAsJsonObject().get("list_schedule");
                Type roomType = new TypeToken<List<Schedule>>() {}.getType();

                List<Schedule> list = gson.fromJson(json, roomType);
                Collections.sort(list, (schedule, t1) -> schedule.getDate().compareTo(t1.getDate()));

                schedules.clear();
                mySchedules.clear();

                todayHeader = null;
                String curDate = null;
                ArrayList<Schedule> tmpList = new ArrayList<>();
                for (Schedule schedule : list) {
                    if (!schedule.getDate().equals(curDate)) {
                        boolean flag = false;
                        for (Schedule item : tmpList) {
                            if (item.getStaffID() == me.getStaffID()) {
                                flag = true;
                                break;
                            }
                        }

                        if (flag) {
                            mySchedules.add(new DateHeader(curDate));
                            mySchedules.addAll(tmpList);
                        }
                        tmpList = new ArrayList<>();

                        curDate = schedule.getDate();
                        DateHeader header = new DateHeader(schedule.getDate());
                        if (today.equals(curDate)) {
                            todayHeader = header;
                        }

                        schedules.add(header);
                    }

                    schedules.add(schedule);
                    tmpList.add(schedule);
                    Log.d(TAG, "onResponse: " + schedule);
                }

                adapter.notifyDataSetChanged();

                Log.d(TAG, "onResponse: " + schedules.indexOf(todayHeader));
                try {
                    if (todayHeader != null) {
                        if (checkBox.isChecked()) {
                            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(mySchedules.indexOf(todayHeader), 0);
                        } else {
                            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(schedules.indexOf(todayHeader), 0);
                        }
                    } else {
                        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(0, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                if (getContext() != null) {
                    Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void backToLogin() {
        if (getActivity() == null) {
            return;
        }

        Log.d(TAG, "backToLogin: ");

        if (getContext() != null) {
            Toast.makeText(getActivity(), R.string.error_unauthorized, Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}
