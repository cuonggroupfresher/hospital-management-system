package com.android.hospitalapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.activity.Insert2BedActivity;
import com.android.hospitalapp.activity.LoginActivity;
import com.android.hospitalapp.adapter.BedAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Bed;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Room;
import com.android.hospitalapp.service.ApiService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BedFragment extends Fragment implements BedAdapter.BedListener {

    private static final String TAG = BedFragment.class.getName();

    private TextView txtTitle;
    private AutoCompleteTextView edtFilter;
    private RecyclerView recyclerView;
    private BedAdapter adapter;

    private ActivityResultLauncher<Intent> resultLauncher;

    private List<Item> allItems;
    private List<Item> items;
    private String token, filter;

    public BedFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {}

        resultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onActivityResult);

        allItems = new ArrayList<>();
        items = new ArrayList<>();

        Log.d(TAG, "onCreate: Created");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Created");

        View view = inflater.inflate(R.layout.fragment_bed, container, false);

        txtTitle = view.findViewById(R.id.txt_title);
        txtTitle.setText(R.string.menu_bed);

        edtFilter = view.findViewById(R.id.edt_filter);

        view.findViewById(R.id.btn_logout).setOnClickListener(v -> logout());

        adapter = new BedAdapter(getActivity(), items, this);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        getData();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.bed_filter));
        edtFilter.setAdapter(adapter);
        edtFilter.setText(getString(R.string.all_bed), false);
        edtFilter.setOnItemClickListener((adapterView, v, position, l) -> {
            updateFilter(adapterView.getItemAtPosition(position).toString());
        });

        return view;
    }

    @Override
    public void onAddClick(int position) {
        Log.d(TAG, "onItemAddClick: " + position);

        if (items.get(position) instanceof Bed) {
            Bed bed = (Bed) items.get(position);
            Intent intent = new Intent(getActivity(), Insert2BedActivity.class);
            intent.putExtra("bed", bed);
            resultLauncher.launch(intent);
        } else {
            Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCheckoutClick(int position) {
        if (items.get(position) instanceof Bed && getActivity() != null) {
            Bed bed = (Bed) items.get(position);

            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Xuất viện");
            alert.setMessage("Bạn có chắc muốn cho người này xuất viện?");
            alert.setPositiveButton(android.R.string.yes, (dialog, which) -> checkOut(bed));
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        } else {
            Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemClick(Bed bed) {
        Intent intent = new Intent(getActivity(), Insert2BedActivity.class);
        intent.putExtra("bed", bed);
        intent.putExtra("mode", 1);
        startActivity(intent);
    }

    private void updateFilter(String str) {
        Log.d(TAG, "filter: " + str);

        if (filter != null && filter.equals(str)) {
            return;
        }

        filter = str;
        if (filter.equals(getString(R.string.empty_bed))) {
            items = new ArrayList<>();

            for (Item item : allItems) {
                if (item instanceof Room) {
                    items.add(item);
                } else if (item instanceof Bed && ((Bed) item).getBedPatientID() == 0) {
                    items.add(item);
                }
            }
        } else if (filter.equals(getString(R.string.full_bed))) {
            items = new ArrayList<>();

            for (Item item : allItems) {
                if (item instanceof Room) {
                    items.add(item);
                } else if (item instanceof Bed && ((Bed) item).getBedPatientID() > 0) {
                    items.add(item);
                }
            }
        } else {
            items = new ArrayList<>(allItems);
        }

        for (int i = 0; i < items.size() - 1; i++) {
            if (items.get(i) instanceof Room && items.get(i + 1) instanceof Room) {
                items.remove(i);
                i--;
            }
        }
        if (items.get(items.size() - 1) instanceof Room) {
            items.remove(items.size() - 1);
        }

        adapter.setItems(items);
    }

    private void checkOut(Bed bed) {
        if (bed.getBedPatientID() > 0) {
            Gson gson = new Gson();
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("bed_patient_id", gson.toJsonTree(bed.getBedPatientID()));
            ApiService.apiService.checkoutBed(token, jsonObject).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (getActivity() == null || response.body() == null) {
                        Log.d(TAG, "onResponse: " + response);
                        try {
                            Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {}
                        return;
                    }

                    Log.d(TAG, "onResponse: " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        try {
                            Toast.makeText(getActivity(), response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {}
                        return;
                    }

                    bed.checkOut();
                    if (!filter.equals(getString(R.string.all_bed))) {
                        items.remove(bed);
                    }

                    adapter.notifyDataSetChanged();

                    update(bed);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                    try {
                        Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {}
                }
            });
        }
    }

    private void update(Bed bed) {
        int index = allItems.indexOf(bed);
        if (index != -1) {
            allItems.set(index, bed);
        }
    }

    private void logout() {
        if (getActivity() != null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Đăng xuất");
            alert.setMessage("Bạn có chắc muốn đăng xuất?");
            alert.setPositiveButton(android.R.string.yes, (dialog, which) -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            });
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        }
    }

    private void getData() {
        if (getActivity() == null) {
            return;
        }

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }

        Log.d(TAG, "getData: token = " + token);

        ApiService.apiService.getRooms(token, new JsonObject()).enqueue(callback);
    }

    private void onActivityResult(ActivityResult result) {
        Log.d(TAG, "onActivityResult: " + result);

        if (result != null && result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            Log.d(TAG, "onActivityResult: " + result.getData());

            Bed bed = (Bed) result.getData().getSerializableExtra("data");
            if (bed == null) {
                return;
            }

            Log.d(TAG, "onActivityResult: " + bed);

            int index = items.indexOf(bed);
            if (index != -1) {
                if (filter.equals(getString(R.string.all_bed))) {
                    items.set(index, bed);
                } else {
                    items.remove(bed);
                }
                adapter.notifyDataSetChanged();

                update(bed);
            }
        }
    }

    private void backToLogin() {
        if (getActivity() == null) {
            return;
        }

        Log.d(TAG, "backToLogin: ");

        try {
            Toast.makeText(getContext(), R.string.error_unauthorized, Toast.LENGTH_SHORT).show();
        } catch (Exception ignored) {}

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    private final Callback<JsonObject> callback = new Callback<JsonObject>() {

        @Override
        public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
            if (response.code() == 401) {
                backToLogin();
                return;
            }

            if (getActivity() == null || response.body() == null) {
                Log.d(TAG, "onResponse: " + response);

                try {
                    Toast.makeText(getContext(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {}
                return;
            }

            Log.d(TAG, "onResponse: " + response.body());

            if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                try {
                    Toast.makeText(getContext(), response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {}
                return;
            }

            Gson gson = new Gson();
            JsonElement json = response.body().get("results").getAsJsonObject().get("list_result");
            Type roomType = new TypeToken<List<Room>>() {}.getType();
            Type bedType = new TypeToken<List<Bed>>() {}.getType();

            List<Room> rooms = gson.fromJson(json, roomType);
            List<Item> temp = new ArrayList<>();
            for (Room room : rooms) {
                temp.add(room);

                List<Bed> beds = gson.fromJson(room.getBedList(), bedType);
                for (Bed bed : beds) {
                    bed.setRoomID(room.getId());
                    bed.setRoom(room.getNumber());
                    bed.setRoomType(room.getGender());
                    temp.add(bed);
                }
            }

            for (Item item : temp) {
                Log.d(TAG, "onResponse: " + item);
            }

            if (allItems != null) {
                allItems.addAll(temp);
            }

            updateFilter(getString(R.string.all_bed));
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            t.printStackTrace();

            try {
                Toast.makeText(getContext(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
            } catch (Exception ignored) {}
        }
    };
}
