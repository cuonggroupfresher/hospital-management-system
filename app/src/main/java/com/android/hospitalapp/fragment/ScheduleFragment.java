package com.android.hospitalapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.activity.LoginActivity;
import com.android.hospitalapp.activity.ScheduleActivity;
import com.android.hospitalapp.adapter.ScheduleAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Schedule;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleFragment extends Fragment {

    private static final String TAG = ScheduleFragment.class.getName();

    private MaterialCalendarView calendar;
    private RecyclerView recyclerView;
    private ScheduleAdapter adapter;

    private ActivityResultLauncher<Intent> resultLauncher;

    private List<Schedule> schedules;
    private Staff staff;
    private String token;

    private ScheduleFragmentListener listener;

    public ScheduleFragment(ScheduleFragmentListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {}

        if (getActivity() != null) {
            resultLauncher = getActivity().registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onActivityResult);
        }

        schedules = new ArrayList<>();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        try {
            staff = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (staff == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: Created");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Created");

        View view = inflater.inflate(R.layout.fragment_schedule, container, false);

        view.findViewById(R.id.btn_logout).setOnClickListener(v -> logout());

        calendar = view.findViewById(R.id.calendar);
        calendar.setTileWidthDp(44);
        calendar.setTileHeightDp(36);
        calendar.setOnDateChangedListener((widget, date, selected) -> {
            Log.d(TAG, "onDateSelected: " + selected + ", " + date);
            updateSchedules();
        });
        calendar.setOnMonthChangedListener((widget, date) -> {
            Log.d(TAG, "onMonthChanged: " + date);
            getSchedules(date.getMonth(), date.getYear());
        });

        adapter = new ScheduleAdapter(getActivity(), new ArrayList<>(), this::onItemClick);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        view.findViewById(R.id.btn_switch).setOnClickListener(view12 -> {
            if (listener != null) {
                listener.onSwitchUI();
            }
        });

        CalendarDay date = calendar.getCurrentDate();
        Log.d(TAG, "onCreateView: " + date);
        getSchedules(date.getMonth(), date.getYear());

        return view;
    }

    private void onActivityResult(ActivityResult result) {
        Log.d(TAG, "onActivityResult: " + result);

        if (result != null && result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            Log.d(TAG, "onActivityResult: " + result.getData());

            Schedule schedule = (Schedule) result.getData().getSerializableExtra("data");
            if (schedule == null) {
                return;
            }

            Log.d(TAG, "onActivityResult: " + schedule);

            int index = schedules.indexOf(schedule);
            if (index != -1) {
                schedules.set(index, schedule);
                updateSchedules();
            }

            Toast.makeText(getContext(), "Cập nhật lịch trực thành công", Toast.LENGTH_SHORT).show();
        }
    }

    private void logout() {
        if (getActivity() != null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Đăng xuất");
            alert.setMessage("Bạn có chắc muốn đăng xuất?");
            alert.setPositiveButton(android.R.string.yes, (dialog, which) -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            });
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        }
    }

    private void onItemClick(Schedule schedule) {
        Log.d(TAG, "onItemClick: " + schedule);

        Intent intent = new Intent(getActivity(), ScheduleActivity.class);
        intent.putExtra("schedule", schedule);
        resultLauncher.launch(intent);
    }

    private void getSchedules(int month, int year) {
        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("month", gson.toJsonTree(month));
        body.add("year", gson.toJsonTree(year));
        ApiService.apiService.getSchedules(token, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (getActivity() == null || response.body() == null) {
                    Log.d(TAG, "onResponse: " + response + ", " + response.body());

                    try {
                        Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {}
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    try {
                        Toast.makeText(getActivity(), response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {}
                    return;
                }

                Gson gson = new Gson();
                JsonElement json = response.body().get("results").getAsJsonObject().get("list_schedule");
                Type roomType = new TypeToken<List<Schedule>>() {}.getType();

                List<Schedule> list = gson.fromJson(json, roomType);
                for (Schedule schedule : list) {
                    Log.d(TAG, "onResponse: " + schedule);
                }

                ScheduleFragment.this.schedules.clear();
                ScheduleFragment.this.schedules.addAll(list);

                if (calendar.getSelectedDate() == null) {
                    setNow();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
                try {
                    Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {}
            }
        });
    }

    private void setNow() {
        CalendarDay today = CalendarDay.today();
        calendar.setSelectedDate(today);
        this.calendar.invalidate();
        updateSchedules();
    }

    private void updateSchedules() {
        CalendarDay date = calendar.getSelectedDate();
        if (date == null) {
            return;
        }

        String dateStr = date.getYear() + "-" +
                (date.getMonth() > 9 ? date.getMonth() : ("0" + date.getMonth())) + "-" +
                (date.getDay() > 9 ? date.getDay() : ("0" + date.getDay()));
        Log.d(TAG, "updateSchedules: " + dateStr);
        List<Schedule> list = new ArrayList<>();
        for (Schedule schedule : schedules) {
            if (dateStr.equals(schedule.getDate())) {
                list.add(schedule);
            }
        }

        if (adapter != null) {
            adapter.setSchedules(list);
        }
    }

    private void backToLogin() {
        if (getActivity() == null) {
            return;
        }

        Log.d(TAG, "backToLogin: ");

        Toast.makeText(getActivity(), R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}
