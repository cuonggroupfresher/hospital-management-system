package com.android.hospitalapp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.hospitalapp.R;
import com.android.hospitalapp.activity.AppointmentActivity;
import com.android.hospitalapp.activity.LoginActivity;
import com.android.hospitalapp.adapter.AppointmentAdapter;
import com.android.hospitalapp.adapter.StaffArrayAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Appointment;
import com.android.hospitalapp.model.DateHeader;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentFragment extends Fragment {

    private static final String TAG = AppointmentFragment.class.getName();

    private AutoCompleteTextView edtDoctor, edtSearch;
    private StaffArrayAdapter staffArrayAdapter, staffArrayAdapter1;
    private RecyclerView recyclerView;
    private AppointmentAdapter adapter;

    private ActivityResultLauncher<Intent> resultLauncher;

    private List<Item> items;
    private Staff me, doctor;
    private String token;

    public AppointmentFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {}

        if (getActivity() != null) {
            resultLauncher = getActivity().registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onActivityResult);
        }

        items = new ArrayList<>();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        try {
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (me == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: Created");
    }

    @SuppressLint({"SimpleDateFormat", "SetTextI18n"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Created");

        View view = inflater.inflate(R.layout.fragment_appointment, container, false);

        view.findViewById(R.id.btn_logout).setOnClickListener(v -> logout());

        edtSearch = view.findViewById(R.id.edt_search);

        edtDoctor = view.findViewById(R.id.edt_doctor);
        edtDoctor.setClickable(false);
        edtDoctor.setFocusable(false);
        edtDoctor.setFocusableInTouchMode(false);

        adapter = new AppointmentAdapter(getActivity(), items, this::onItemClick);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (me != null) {
            edtSearch.setText(me.toString());
            edtDoctor.setText(me.toString());
            if (!"1".equals(me.getRoleID())) {
                edtSearch.setVisibility(View.GONE);
                view.findViewById(R.id.layout_search).setVisibility(View.GONE);
            }

            if ("1".equals(me.getRoleID())) {
                getDoctors();
            } else if ("2".equals(me.getRoleID())) {
                doctor = me;
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                getAppointments(getCurrentWeekOfYear(), calendar.get(Calendar.YEAR));
            }
        } else {
            backToLogin();
        }

        return view;
    }

    private void onActivityResult(ActivityResult result) {
        Log.d(TAG, "onActivityResult: " + result);

        if (result != null && result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
            Log.d(TAG, "onActivityResult: " + result.getData());

            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            getAppointments(getCurrentWeekOfYear(), calendar.get(Calendar.YEAR));
        }
    }

    private void logout() {
        if (getActivity() != null) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Đăng xuất");
            alert.setMessage("Bạn có chắc muốn đăng xuất?");
            alert.setPositiveButton(android.R.string.yes, (dialog, which) -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().finish();
            });
            alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
            alert.show();
        }
    }

    private void onItemClick(Appointment appointment) {
        Log.d(TAG, "onItemClick: " + appointment);

        if (appointment != null) {
            Intent intent = new Intent(getActivity(), AppointmentActivity.class);
            intent.putExtra("appointment", appointment);
            resultLauncher.launch(intent);
        }
    }

    private void getDoctors() {
        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("role_id", gson.toJsonTree(2));

        ApiService.apiService.getStaffs(token, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (getActivity() == null || response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                    return;
                }

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Staff>>() {}.getType();

                List<Staff> list = new Gson().fromJson(json, listType);
                for (Staff staff : list) {
                    Log.d(TAG, "onResponse: " + staff);
                    if (me.equals(staff)) {
                        AppointmentFragment.this.doctor = staff;
                    }
                }

                Log.d(TAG, "onResponse: " + doctor);

                staffArrayAdapter1 = new StaffArrayAdapter(getActivity(), R.layout.simple_text_view, list);
                staffArrayAdapter1.setNotifyOnChange(true);
                edtDoctor.setAdapter(staffArrayAdapter1);
                edtDoctor.setOnItemClickListener((adapterView, view, i, l) -> {
                    if (adapterView.getItemAtPosition(i) instanceof Staff) {
                        doctor = (Staff) adapterView.getItemAtPosition(i);
                        Log.d(TAG, "onItemSelected1: doctor " + doctor);
                        edtSearch.setText(doctor.toString());
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        getAppointments(getCurrentWeekOfYear(), calendar.get(Calendar.YEAR));
                    } else {
                        doctor = null;
                    }
                });

                staffArrayAdapter = new StaffArrayAdapter(getActivity(), R.layout.simple_text_view, list);
                edtSearch.setAdapter(staffArrayAdapter);
                edtSearch.setOnItemClickListener((adapterView, view, position, l) -> {
                    if (adapterView.getItemAtPosition(position) instanceof Staff) {
                        doctor = (Staff) adapterView.getItemAtPosition(position);
                        staffArrayAdapter.getFilter().filter(doctor.toString());
                        Log.d(TAG, "onItemSelected: doctor " + doctor);
                        edtDoctor.setText(doctor.toString(), false);
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        getAppointments(getCurrentWeekOfYear(), calendar.get(Calendar.YEAR));
                    } else {
                        doctor = null;
                    }
                });

                if (doctor != null) {
                    edtSearch.setText(doctor.toString());
                    edtDoctor.setText(doctor.toString(), false);
                } else if (!list.isEmpty()) {
                    doctor = list.get(0);
                    edtSearch.setText(doctor.toString());
                    edtDoctor.setText(doctor.toString(), false);
                }

                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                getAppointments(getCurrentWeekOfYear(), calendar.get(Calendar.YEAR));
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAppointments(int weekOfYear, int year) {
        if (doctor == null) {
            return;
        }

        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("staff_id", gson.toJsonTree(doctor.getStaffID()));

        body.add("week", gson.toJsonTree(weekOfYear));
        body.add("year", gson.toJsonTree(year));

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String today = format.format(calendar.getTime());

        Log.d(TAG, "getAppointments: " + weekOfYear + ", " + calendar.getFirstDayOfWeek());
        ApiService.apiService.getAppointments(token, body).enqueue(new Callback<JsonObject>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (getActivity() == null || response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                    return;
                }

                List<Appointment> list = new ArrayList<>();
                Type listType = new TypeToken<List<Appointment>>() {}.getType();

                JsonArray array = response.body().get("results").getAsJsonArray();
                for (JsonElement element : array) {
                    Log.d(TAG, "onResponse: " + element);

                    JsonArray appointments = element.getAsJsonObject().get("list_appointment_day").getAsJsonArray();
                    if (appointments != null && !appointments.isEmpty()) {
                        list.addAll(gson.fromJson(appointments, listType));
                    }
                }

                Collections.sort(list, (a1, a2) -> {
                    int result = a1.getDate().compareTo(a2.getDate());
                    if (result != 0) {
                        return result;
                    }

                    return a1.getTime().compareTo(a2.getTime());
                });

                AppointmentFragment.this.items.clear();
                DateHeader todayHeader = null;
                String curDate = null;
                for (Appointment appointment : list) {
                    Log.d(TAG, "onResponse: " + today + " - " + appointment.getDate());
                    if (!appointment.getDate().equals(curDate)) {
                        curDate = appointment.getDate();
                        DateHeader header = new DateHeader(appointment.getDate());
                        if (today.equals(curDate)) {
                            todayHeader = header;
                        }
                        AppointmentFragment.this.items.add(header);
                    }

                    AppointmentFragment.this.items.add(appointment);
                    Log.d(TAG, "onResponse: " + appointment);
                }

                adapter.notifyDataSetChanged();

                Log.d(TAG, "onResponse: " + items.indexOf(todayHeader));
                try {
                    if (todayHeader != null) {
                        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(items.indexOf(todayHeader), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(getActivity(), R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int getCurrentWeekOfYear() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int weekOfYear;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            LocalDate date = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            weekOfYear = date.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
        } else {
            weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        }

        return weekOfYear;
    }

    private void backToLogin() {
        if (getActivity() == null) {
            return;
        }

        Log.d(TAG, "backToLogin: ");

        Toast.makeText(getActivity(), R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}
