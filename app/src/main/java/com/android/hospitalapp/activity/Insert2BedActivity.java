package com.android.hospitalapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.android.hospitalapp.R;
import com.android.hospitalapp.adapter.PatientArrayAdapter;
import com.android.hospitalapp.adapter.StaffArrayAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Bed;
import com.android.hospitalapp.model.Patient;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Insert2BedActivity extends AppCompatActivity {

    private static final String TAG = Insert2BedActivity.class.getName();

    private AutoCompleteTextView edtDoctor, edtPatient;
    private TextInputLayout edtRoom, edtBed, edtDetails, edtDate;
    private Staff doctor;
    private Patient patient;
    private Bed bed;
    private String token;
    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert2_bed);

        edtDoctor = findViewById(R.id.edt_doctor);
        edtPatient = findViewById(R.id.edt_patient);
        edtRoom = findViewById(R.id.edt_room);
        edtBed = findViewById(R.id.edt_bed);
        edtDetails = findViewById(R.id.edt_details);
        edtDate = findViewById(R.id.edt_date);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.insert_to_bed);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        bed = (Bed) getIntent().getSerializableExtra("bed");
        if (bed == null) {
            Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
            Insert2BedActivity.this.finish();
            return;
        }

        mode = getIntent().getIntExtra("mode", 0);

        edtRoom.getEditText().setText(String.valueOf(bed.getRoom()));
        edtBed.getEditText().setText(bed.getName());

        if (mode == 0) {
            edtDate.setVisibility(View.GONE);

            getDoctors();
            getPatients();
        } else {
            edtDate.setVisibility(View.VISIBLE);

            edtDoctor.setClickable(false);
            edtDoctor.setFocusable(false);
            edtDoctor.setFocusableInTouchMode(false);

            edtPatient.setClickable(false);
            edtPatient.setFocusable(false);
            edtPatient.setFocusableInTouchMode(false);

            edtDetails.getEditText().setClickable(false);
            edtDetails.getEditText().setFocusable(false);
            edtDetails.getEditText().setFocusableInTouchMode(false);

            edtDoctor.setText(bed.getStaffIDUser() + " - " + bed.getStaffName());
            edtPatient.setText(bed.getPatientIDUser() + " - " + bed.getPatientName());
            edtDetails.getEditText().setText(bed.getDescription());

            String date = bed.getDateIn().replace("T", " ").replace("Z","");
            Log.d(TAG, "onCreate: " + date);
            try {
                date = date.substring(0, date.lastIndexOf("."));
            } catch (StringIndexOutOfBoundsException e){
                e.printStackTrace();
            }


            edtDate.getEditText().setText(date);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mode != 0) {
            return super.onCreateOptionsMenu(menu);
        }

        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Insert2BedActivity.this.finish();
            return true;
        } else if (item.getItemId() == R.id.menu_save) {
            save();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getDoctors() {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("role_id", gson.toJsonTree(2));

        ApiService.apiService.getStaffs(token, jsonObject).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    Insert2BedActivity.this.finish();
                    return;
                }

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Staff>>() {}.getType();

                List<Staff> list = new Gson().fromJson(json, listType);
                for (Staff staff : list) {
                    Log.d(TAG, "onResponse: " + staff);
                }

                StaffArrayAdapter adapter = new StaffArrayAdapter(Insert2BedActivity.this, R.layout.simple_text_view, list);
                edtDoctor.setAdapter(adapter);
                edtDoctor.setOnItemClickListener((adapterView, view, position, l) -> {
                    if (adapterView.getItemAtPosition(position) instanceof Staff) {
                        doctor = (Staff) adapterView.getItemAtPosition(position);
                        Log.d(TAG, "onItemSelected: doctor " + doctor);
                    } else {
                        doctor = null;
                    }
                });
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                Insert2BedActivity.this.finish();
            }
        });
    }

    private void getPatients() {
        ApiService.apiService.getPatients(token, new JsonObject()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    Insert2BedActivity.this.finish();
                    return;
                }

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Patient>>() {}.getType();

                List<Patient> list = new Gson().fromJson(json, listType);
                for (Patient patient : list) {
                    Log.d(TAG, "onResponse: patient " + patient);
                }

                PatientArrayAdapter adapter = new PatientArrayAdapter(Insert2BedActivity.this, R.layout.simple_text_view, list);
                edtPatient.setAdapter(adapter);
                edtPatient.setOnItemClickListener((adapterView, view, position, l) -> {
                    if (adapterView.getItemAtPosition(position) instanceof Patient) {
                        patient = (Patient) adapterView.getItemAtPosition(position);
                        Log.d(TAG, "onItemSelected: patient " + patient);
                    } else {
                        patient = null;
                    }
                });
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                Insert2BedActivity.this.finish();
            }
        });
    }
    
    private void save() {
        if (doctor == null || patient == null || edtRoom.getEditText() == null || edtRoom.getEditText().getText().toString().isEmpty() ||
                edtBed.getEditText() == null || edtBed.getEditText().getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!patient.getGender().equals(bed.getRoomType())) {
            Toast.makeText(this, "Giới tính không hợp lệ", Toast.LENGTH_SHORT).show();
            return;
        }

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("staff", gson.toJsonTree(doctor.getStaffID()));
        jsonObject.add("patient", gson.toJsonTree(patient.getPatientID()));
        jsonObject.add("room", gson.toJsonTree(bed.getRoomID()));
        jsonObject.add("bed", gson.toJsonTree(Integer.parseInt(bed.getId())));
        if (edtDetails.getEditText() != null) {
            jsonObject.add("description", gson.toJsonTree(edtDetails.getEditText().getText().toString()));
        }

        ApiService.apiService.insertToBed(token, jsonObject).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Toast.makeText(Insert2BedActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    return;
                }

                JsonObject object = response.body().get("results").getAsJsonObject();
                bed.setBedPatientID(object.get("id").getAsInt());

                bed.setStaffID(doctor.getStaffID());
                bed.setStaffName(doctor.getFullname());
                bed.setStaffPhone(doctor.getPhoneNumber());
                bed.setStaffIDUser(doctor.getId());

                bed.setPatientID(patient.getPatientID());
                bed.setPatientName(patient.getFullname());
                bed.setPatientPhone(patient.getPhoneNumber());
                bed.setPatientIDUser(patient.getId());

                bed.setDateIn(object.get("date_in").getAsString());
                bed.setCount(0);

                Log.d(TAG, "onResponse: " + bed);
                Intent result = new Intent();
                result.putExtra("data", bed);
                setResult(RESULT_OK, result);
                Insert2BedActivity.this.finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(Insert2BedActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void backToLogin() {
        Toast.makeText(this, R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
    }
}