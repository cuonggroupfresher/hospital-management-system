package com.android.hospitalapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.android.hospitalapp.R;
import com.android.hospitalapp.adapter.StaffArrayAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Schedule;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleActivity extends AppCompatActivity {

    private static final String TAG = ScheduleActivity.class.getName();

    private TextInputLayout edtStaff, edtDetail, edtDate;
    private Staff me;
    private Staff staff;
    private Schedule schedule;
    private String token;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        edtStaff = findViewById(R.id.edt_staff);
        edtDetail = findViewById(R.id.edt_details);
        edtDate = findViewById(R.id.edt_date);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.update_schedule);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        try {
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (me == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: " + me);

        schedule = (Schedule) getIntent().getSerializableExtra("schedule");
        if (schedule == null) {
            Toast.makeText(ScheduleActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
            ScheduleActivity.this.finish();
            return;
        }
        Log.d(TAG, "onCreate: " + schedule);

        edtDate.getEditText().setText(schedule.getDate());
        edtDetail.getEditText().setText(schedule.getDescription());
        if (schedule.getRoleID().equals("2")) {
            edtStaff.setHint(R.string.doctor);
        } else if (schedule.getRoleID().equals("3")) {
            edtStaff.setHint(R.string.nurse);
        }

        if (me.getRoleID().equals("1")) {
            getStaffs();
        } else {
            actionBar.setTitle("Chi tiết lịch trực");

            edtStaff.getEditText().setClickable(false);
            edtStaff.getEditText().setFocusable(false);
            edtStaff.getEditText().setFocusableInTouchMode(false);
            edtStaff.getEditText().setText(schedule.getId() + " - " + schedule.getName());

            edtDetail.getEditText().setClickable(false);
            edtDetail.getEditText().setFocusable(false);
            edtDetail.getEditText().setFocusableInTouchMode(false);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (me != null && me.getRoleID().equals("1")) {
            getMenuInflater().inflate(R.menu.menu_save, menu);
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ScheduleActivity.this.finish();
            return true;
        } else if (item.getItemId() == R.id.menu_save) {
            save();
        }

        return super.onOptionsItemSelected(item);
    }
    
    private void save() {
        if (staff == null || edtStaff.getEditText() == null || edtStaff.getEditText().getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        String description = "";
        if (edtDetail.getEditText() != null && !edtDetail.getEditText().getText().toString().isEmpty()) {
            description = edtDetail.getEditText().getText().toString();
        }

        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("id", gson.toJsonTree(schedule.getId()));
        body.add("staff", gson.toJsonTree(staff.getStaffID()));
        body.add("description", gson.toJsonTree(description));

        ApiService.apiService.updateSchedule(token, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(ScheduleActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Toast.makeText(ScheduleActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    return;
                }

                JsonObject object = response.body().get("results").getAsJsonObject();
                schedule.setDescription(object.get("description").getAsString());

                schedule.setUserID(staff.getId());
                schedule.setName(staff.getFullname());
                schedule.setPhoneNumber(staff.getPhoneNumber());
                schedule.setImage(staff.getImage());
                schedule.setStaff(staff.isStaff());
                schedule.setOtp(staff.getOtp());
                schedule.setAddress(staff.getAddress());
                schedule.setGender(staff.getGender());
                schedule.setGenderName(staff.getGenderName());
                schedule.setBirthday(staff.getBirthday());
                schedule.setStaffID(staff.getStaffID());
                schedule.setRoleID(staff.getRoleID());
                schedule.setRoleName(staff.getRoleName());
                schedule.setPosition(staff.getPosition());
                schedule.setIntroduction(staff.getIntroduction());

                Log.d(TAG, "onResponse: " + schedule);
                Intent result = new Intent();
                result.putExtra("data", schedule);
                setResult(RESULT_OK, result);
                ScheduleActivity.this.finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(ScheduleActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getStaffs() {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("role_id", gson.toJsonTree(Integer.parseInt(schedule.getRoleID())));

        ApiService.apiService.getStaffs(token, jsonObject).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(ScheduleActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    ScheduleActivity.this.finish();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                    ScheduleActivity.this.finish();
                    return;
                }

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Staff>>() {}.getType();

                List<Staff> list = new Gson().fromJson(json, listType);
                for (Staff staff : list) {
                    Log.d(TAG, "onResponse: " + staff);
                    if (schedule.getStaffID() == staff.getStaffID() && schedule.getUserID().equals(staff.getId())) {
                        ScheduleActivity.this.staff = staff;
                    }
                }

                StaffArrayAdapter adapter = new StaffArrayAdapter(ScheduleActivity.this, R.layout.simple_text_view, list);
                if (edtStaff.getEditText() instanceof AutoCompleteTextView) {
                    AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) edtStaff.getEditText();
                    autoCompleteTextView.setAdapter(adapter);
                    autoCompleteTextView.setOnItemClickListener((adapterView, view, position, l) -> {
                        if (adapterView.getItemAtPosition(position) instanceof Staff) {
                            staff = (Staff) adapterView.getItemAtPosition(position);
                            Log.d(TAG, "onItemSelected: staff " + staff);
                        } else {
                            staff = null;
                        }
                    });
                    Log.d(TAG, "onResponse: " + staff);
                    if (staff != null) {
                        autoCompleteTextView.setText(staff.toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(ScheduleActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                ScheduleActivity.this.finish();
            }
        });
    }

    private void backToLogin() {
        Toast.makeText(this, R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
    }
}