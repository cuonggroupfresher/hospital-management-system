package com.android.hospitalapp.activity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.hospitalapp.model.NotificationModel;
import com.android.hospitalapp.util.AlarmUtils;
import com.android.hospitalapp.R;
import com.android.hospitalapp.adapter.AppointmentAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Appointment;
import com.android.hospitalapp.model.DateHeader;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Patient;
import com.android.hospitalapp.service.ApiService;
import com.android.hospitalapp.service.SchedulingService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientActivity extends AppCompatActivity {

    private static final String TAG = PatientActivity.class.getName();

    private TextInputLayout edtID, edtName, edtPhone, edtBirthday, edtAddress, edtPassword;
    private RadioButton radioMale, radioFemale, radioOther;
    private RecyclerView recyclerViewHistory;
    private RecyclerView recyclerViewAppointments;
    private AppointmentAdapter historyAdapter, appointmentAdapter;

    private ActivityResultLauncher<Intent> mResultLauncher;

    private final Calendar myCalendar = Calendar.getInstance();

    private boolean doubleBackToExitPressedOnce = false;

    private List<Item> items, itemsHistory;
    private String token;
    private Patient patient;
    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        mResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onRequestResult);

        AlarmUtils.clean(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                mResultLauncher.launch(intent);
            }
        }

        mode = 0;

        items = new ArrayList<>();
        itemsHistory = new ArrayList<>();

        edtID = findViewById(R.id.edt_id);
        edtName = findViewById(R.id.edt_name);
        edtPhone = findViewById(R.id.edt_phone);
        edtBirthday = findViewById(R.id.edt_birthday);
        edtAddress = findViewById(R.id.edt_address);
        edtPassword = findViewById(R.id.edt_password);
        radioMale = findViewById(R.id.radio_male);
        radioFemale = findViewById(R.id.radio_female);
        radioOther = findViewById(R.id.radio_other);

        appointmentAdapter = new AppointmentAdapter(this, items, this::onItemClick);
        appointmentAdapter.setPatient(true);

        recyclerViewAppointments = findViewById(R.id.recycler_view_appointments);
        recyclerViewAppointments.setAdapter(appointmentAdapter);
        recyclerViewAppointments.setLayoutManager(new LinearLayoutManager(this));

        historyAdapter = new AppointmentAdapter(this, itemsHistory, this::onItemClick);
        historyAdapter.setPatient(true);

        recyclerViewHistory = findViewById(R.id.recycler_view_history);
        recyclerViewHistory.setAdapter(historyAdapter);
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(this));

        mode = getIntent().getIntExtra("mode", 0);

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        ActionBar actionBar = getSupportActionBar();
        if (this.mode == 2) {
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(R.string.add_patient);
            }

            edtID.setVisibility(View.GONE);
        } else if (this.mode == 1) {
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(R.string.patient);
            }

            int patientID = getIntent().getIntExtra("patient_id", -1);
            if (patientID == -1) {
                Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                PatientActivity.this.finish();
                return;
            }

            ApiService.apiService.getPatient(patientID, token).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (response.body() == null) {
                        Log.d(TAG, "onResponse: " + response);

                        Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                        PatientActivity.this.finish();
                        return;
                    }

                    Log.d(TAG, "onResponse: " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        Toast.makeText(PatientActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        PatientActivity.this.finish();
                        return;
                    }

                    Gson gson = new Gson();
                    JsonObject object = response.body().get("results").getAsJsonObject();
                    patient = gson.fromJson(object, Patient.class);
                    updatePatient();

                    Type listType = new TypeToken<List<Appointment>>() {}.getType();
                    List<Appointment> appointments = gson.fromJson(object.get("list_appointment"), listType);
                    updateAppointments(appointments);

                    List<Appointment> appointmentsHistory = gson.fromJson(object.get("list_history_appointment"), listType);
                    updateAppointmentsHistory(appointmentsHistory);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    PatientActivity.this.finish();
                }
            });
        } else {
            if (actionBar != null) {
                actionBar.setTitle(R.string.patient);
            }

            try {
                patient = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Patient.class);

                ApiService.apiService.getPatient(patient.getPatientID(), token).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.code() == 401) {
                            backToLogin();
                            return;
                        }

                        if (response.body() == null) {
                            Log.d(TAG, "onResponse: " + response);

                            Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Log.d(TAG, "onResponse: " + response.body());

                        if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                            Toast.makeText(PatientActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Gson gson = new Gson();
                        JsonObject object = response.body().get("results").getAsJsonObject();
                        patient = gson.fromJson(object, Patient.class);
                        updatePatient();

                        Type listType = new TypeToken<List<Appointment>>() {}.getType();
                        List<Appointment> appointments = gson.fromJson(object.get("list_appointment"), listType);
                        updateAppointments(appointments);
                        List<Appointment> appointmentsHistory = gson.fromJson(object.get("list_history_appointment"), listType);
                        updateAppointmentsHistory(appointmentsHistory);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        t.printStackTrace();

                        Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    }
                });

                // updatePatient();
                edtName.getEditText().setClickable(false);
                edtName.getEditText().setFocusable(false);
                edtName.getEditText().setFocusableInTouchMode(false);

                edtPhone.getEditText().setClickable(false);
                edtPhone.getEditText().setFocusable(false);
                edtPhone.getEditText().setFocusableInTouchMode(false);

                edtAddress.getEditText().setClickable(false);
                edtAddress.getEditText().setFocusable(false);
                edtAddress.getEditText().setFocusableInTouchMode(false);

                radioMale.setClickable(false);
                radioFemale.setClickable(false);
                radioOther.setClickable(false);

                edtPassword.getEditText().setClickable(false);
                edtPassword.getEditText().setFocusable(false);
                edtPassword.getEditText().setFocusableInTouchMode(false);
            } catch (Exception e) {
                e.printStackTrace();
                backToLogin();
            }
        }

        DatePickerDialog.OnDateSetListener date = (view, year, month, day) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, day);

            updateLabel();
        };

        if (edtBirthday.getEditText() != null && mode != 0) {
            edtBirthday.getEditText().setOnClickListener(view -> {
                new DatePickerDialog(PatientActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            });
        }

        edtName.clearFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (mode == 0) {
            createNotificationChannel();
            getFutureAppointments();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mode == 0) {
            getMenuInflater().inflate(R.menu.menu_patient_1, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_patient, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.menu_save) {
            save();
        } else if (item.getItemId() == R.id.menu_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mode == 0) {
            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                System.exit(0);
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Nhấn trở về lần nữa để thoát", Toast.LENGTH_SHORT).show();

            new Handler(Looper.getMainLooper()).postDelayed(() -> doubleBackToExitPressedOnce = false, 1500);
        } else {
            super.onBackPressed();
        }
    }

    private void onRequestResult(ActivityResult result) {
        Log.d(TAG, "onRequestResult: " + result);
        if (result != null && result.getResultCode() == RESULT_CANCELED) {
            Toast.makeText(this, "Bạn có thể bỏ lỡ một số thông báo của chúng tôi khi ứng dụng đóng.", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateAppointments(List<Appointment> appointments) {
        if (appointments == null) {
            return;
        }

        Collections.sort(appointments, (a1, a2) -> {
            int result = a1.getDate().compareTo(a2.getDate());
            if (result != 0) {
                return result;
            }

            return a1.getTime().compareTo(a2.getTime());
        });

        this.items.clear();
        String curDate = null;
        for (Appointment appointment : appointments) {
            if (!appointment.getDate().equals(curDate)) {
                curDate = appointment.getDate();
                this.items.add(new DateHeader(appointment.getDate()));
            }

            this.items.add(appointment);
        }

        appointmentAdapter.notifyDataSetChanged();
    }

    private void updateAppointmentsHistory(List<Appointment> appointments) {
        if (appointments == null) {
            return;
        }

        Collections.sort(appointments, (a1, a2) -> {
            int result = a2.getDate().compareTo(a1.getDate());
            if (result != 0) {
                return result;
            }

            return a1.getTime().compareTo(a2.getTime());
        });

        this.itemsHistory.clear();
        String curDate = null;
        for (Appointment appointment : appointments) {
            if (!appointment.getDate().equals(curDate)) {
                curDate = appointment.getDate();
                this.itemsHistory.add(new DateHeader(appointment.getDate()));
            }

            this.itemsHistory.add(appointment);
        }

        historyAdapter.notifyDataSetChanged();
    }

    private void onItemClick(Appointment appointment) {
        Log.d(TAG, "onItemClick: " + appointment);

        if (appointment != null) {
            Intent intent = new Intent(this, AppointmentActivity.class);
            intent.putExtra("appointment", appointment);
            intent.putExtra("mode", 1);
            startActivity(intent);
        }
    }

    private void logout() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Đăng xuất");
        alert.setMessage("Bạn có chắc muốn đăng xuất?");
        alert.setPositiveButton(android.R.string.yes, (dialog, which) -> {
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent myIntent = new Intent(getApplicationContext(), SchedulingService.class);
            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pendingIntent);

            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        });
        alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
        alert.show();
    }

    private static final String CHANNEL_ID = "TA22";
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void getAppointments() {
        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("patient_id", gson.toJsonTree(patient.getPatientID()));
        ApiService.apiService.getTomorrowAppointment(token, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: 1 " + response);
                    return;
                }

                Log.d(TAG, "onResponse: 2 " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: 3 " + response.body().get("message").getAsString());
                    if (response.code() == 213 || response.body().get("code").getAsInt() == 213) {
                        getFutureAppointments();
                    }

                    return;
                }

                JsonArray array = response.body().get("results").getAsJsonArray();
                String notifyStr = "Ngày mai bạn có lịch khám vào lúc:";
                for (JsonElement element : array) {
                    Log.d(TAG, "onResponse: 4 " + element.getAsJsonObject().get("time_display").getAsString());
                    notifyStr += ("\n\t- " + element.getAsJsonObject().get("time_display").getAsString());
                }

                if (!array.isEmpty()) {
                    Log.d(TAG, "onResponse: 5");
                    sendNotification("Lịch khám ngày mai", notifyStr);
                } else {
                    Log.d(TAG, "onResponse: 6");
                    getFutureAppointments();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();
            }
        });

        // sendNotification("Lịch khám", "Y tá: Xem danh sách bác sĩ, y tá. Quản lý bệnh nhân. Xem lịch trực bác sĩ, y tá. Quản lý bệnh nhân theo giường theo phòng");
    }

    private void getFutureAppointments() {
        boolean isTesting = false;

        if (isTesting) {
            Log.d(TAG, "getFutureAppointments: Testing - Push notification after 1 minutes");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.add(Calendar.MINUTE, 2);

            AlarmUtils.create(PatientActivity.this, new NotificationModel(calendar, "Chạy thử hẹn giờ gửi thông báo sau 1 phút"), new NotificationModel(calendar1, "Chạy thử hẹn giờ gửi thông báo sau 2 phút"));
        } else {
            Gson gson = new Gson();
            JsonObject body = new JsonObject();
            body.add("number_phone", gson.toJsonTree(patient.getPhoneNumber()));
            ApiService.apiService.getFutureAppointments(token, body).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (response.body() == null) {
                        Log.d(TAG, "onResponse: 1 " + response);
                        return;
                    }

                    Log.d(TAG, "onResponse: 2 " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        Log.d(TAG, "onResponse: 3 " + response.body().get("message").getAsString());
                        return;
                    }

                    JsonArray array = response.body().get("results").getAsJsonArray();
                    Type roomType = new TypeToken<List<Appointment>>() {}.getType();
                    List<Appointment> list = gson.fromJson(array, roomType);
                    Collections.sort(list, (a1, a2) -> {
                        int tmp = a1.getDate().compareTo(a2.getDate());
                        if (tmp != 0) {
                            return tmp;
                        }

                        return a1.getTimeDisplay().compareTo(a2.getTimeDisplay());
                    });

                    if (!list.isEmpty()) {
                        Calendar tomorrow = Calendar.getInstance();
                        tomorrow.add(Calendar.DATE, 1);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        String tomorrowStr = dateFormat.format(tomorrow.getTime());

                        List<NotificationModel> notificationModels = new ArrayList<>();
                        String current = list.get(0).getDate();
                        if (current == null || current.isEmpty()) {
                            return;
                        }

                        for (int i = 0; i < list.size(); i++) {
                            String notifyStr = "Ngày mai bạn có lịch khám vào lúc:";
                            while (i < list.size() && current.equals(list.get(i).getDate())) {
                                Log.d(TAG, "onResponse: 4 " + current + " : " + tomorrowStr);
                                notifyStr += ("\n   - " + list.get(i).getTimeDisplay());
                                i++;
                            }

                            if (current.equals(tomorrowStr)) {
                                Log.d(TAG, "onResponse: 5");
                                sendNotification("Lịch khám ngày mai", notifyStr);
                            } else {
                                Log.d(TAG, "onResponse: 6");
                                try {
                                    String[] strings = current.split("-");

                                    Calendar time = Calendar.getInstance();
                                    time.set(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]) - 1, Integer.parseInt(strings[2]), 8, 0, 0);
                                    time.add(Calendar.DATE, -1);

                                    notificationModels.add(new NotificationModel(time, notifyStr));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            }

                            if (i < list.size()) {
                                current = list.get(i).getDate();
                                i--;
                            }
                        }
                        AlarmUtils.create(PatientActivity.this, notificationModels);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private void sendNotification(String title, String content) {
        final Intent emptyIntent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, emptyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_foreground))
                .setContentTitle(title)
                .setContentText(content)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setAutoCancel(true);
        builder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(1, builder.build());

        Toast.makeText(PatientActivity.this, content, Toast.LENGTH_SHORT).show();
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.getDefault());
        edtBirthday.getEditText().setText(dateFormat.format(myCalendar.getTime()));
    }

    private void updatePatient() {
        try {
            Log.d(TAG, "updatePatient: patient " + patient);

            edtID.getEditText().setText(patient.getId());
            edtName.getEditText().setText(patient.getFullname());
            edtPhone.getEditText().setText(patient.getPhoneNumber());
            edtBirthday.getEditText().setText(patient.getBirthday());
            edtAddress.getEditText().setText(patient.getAddress());
            if (patient.getGender().equals("1")) {
                radioMale.setChecked(true);
            } else if (patient.getGender().equals("2")) {
                radioFemale.setChecked(true);
            } else {
                radioOther.setChecked(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            PatientActivity.this.finish();
        }
    }

    private void save() {
        if (edtPhone.getEditText() == null || edtPhone.getEditText().getText().toString().isEmpty() ||
                edtName.getEditText() == null || edtName.getEditText().getText().toString().isEmpty() ||
                edtAddress.getEditText() == null || edtAddress.getEditText().getText().toString().isEmpty() ||
                edtBirthday.getEditText() == null || edtBirthday.getEditText().getText().toString().isEmpty() ||
                edtBirthday.getEditText().getText().toString().equals(getString(R.string.date_format))) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mode == 2 && (edtPassword.getEditText() == null || edtPassword.getEditText().getText().toString().isEmpty())) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        String name = edtName.getEditText().getText().toString();
        String phone = edtPhone.getEditText().getText().toString();
        String birthday = edtBirthday.getEditText().getText().toString();
        String address = edtAddress.getEditText().getText().toString();
        int gender = (radioMale.isChecked() ? 1 : (radioFemale.isChecked() ? 2 : 3));

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("fullname", gson.toJsonTree(name));
        jsonObject.add("number_phone", gson.toJsonTree(phone));
        jsonObject.add("gender", gson.toJsonTree(gender));
        jsonObject.add("birthday", gson.toJsonTree(birthday));
        jsonObject.add("address", gson.toJsonTree(address));
        if (edtPassword.getEditText() != null && !edtPassword.getEditText().getText().toString().isEmpty()) {
            jsonObject.add("password", gson.toJsonTree(edtPassword.getEditText().getText().toString()));
        }

        if (mode == 2) {
            ApiService.apiService.createPatient(token, jsonObject).enqueue(callback);
        } else {
            ApiService.apiService.updatePatient(patient.getPatientID(), token, jsonObject).enqueue(callback);
        }
    }

    private final Callback<JsonObject> callback = new Callback<JsonObject>() {

        @Override
        public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
            if (response.code() == 401) {
                backToLogin();
                return;
            }

            if (response.body() == null) {
                Log.d(TAG, "onResponse: " + response);

                Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                return;
            }

            if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                Toast.makeText(PatientActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                return;
            }

            Gson gson = new Gson();
            if (mode == 2) {
                JsonObject object = response.body().get("results").getAsJsonObject().get("info").getAsJsonObject();
                Patient patient = gson.fromJson(object, Patient.class);
                Log.d(TAG, "onResponse: Added " + patient);
                Intent result = new Intent();
                result.putExtra("data", patient);
                setResult(RESULT_OK, result);
                PatientActivity.this.finish();
            } else if (mode == 1) {
                Log.d(TAG, "onResponse: Updated " + response.body());
                JsonObject object = response.body().get("results").getAsJsonObject();
                Patient patient = gson.fromJson(object, Patient.class);
                patient.setPatientID(PatientActivity.this.patient.getPatientID());
                Log.d(TAG, "onResponse: Updated " + patient);
                Intent result = new Intent();
                result.putExtra("data", patient);
                setResult(RESULT_OK, result);
                PatientActivity.this.finish();
            } else {
                Log.d(TAG, "onResponse: Updated " + response.body());
                Toast.makeText(PatientActivity.this, "Cập nhật thành công", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            t.printStackTrace();

            Toast.makeText(PatientActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
        }
    };

    private void backToLogin() {
        Toast.makeText(this, R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
    }
}