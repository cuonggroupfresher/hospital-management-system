package com.android.hospitalapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.hospitalapp.R;
import com.android.hospitalapp.adapter.AppointmentAdapter;
import com.android.hospitalapp.adapter.PatientBedAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.fragment.ManagerFragment;
import com.android.hospitalapp.model.Appointment;
import com.android.hospitalapp.model.Bed;
import com.android.hospitalapp.model.DateHeader;
import com.android.hospitalapp.model.Item;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffActivity extends AppCompatActivity {

    private static final String TAG = StaffActivity.class.getName();

    private TextInputLayout edtID, edtName, edtPhone, edtBirthday, edtAddress, edtPassword, edtPosition, edtIntroduction;
    private TextView txtAppointments, txtPatients;
    private RadioButton radioMale, radioFemale, radioOther;
    private RecyclerView recyclerView;
    private RecyclerView recyclerView2;
    private AppointmentAdapter adapter;
    private PatientBedAdapter patientBedAdapter;

    private final Calendar myCalendar = Calendar.getInstance();

    private List<Item> items;
    private List<Bed> beds;
    private String token;
    private Staff me;
    private Staff staff;
    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);

        mode = 0;

        items = new ArrayList<>();
        beds = new ArrayList<>();

        edtID = findViewById(R.id.edt_id);
        edtName = findViewById(R.id.edt_name);
        edtPhone = findViewById(R.id.edt_phone);
        edtBirthday = findViewById(R.id.edt_birthday);
        edtAddress = findViewById(R.id.edt_address);
        edtPassword = findViewById(R.id.edt_password);
        edtPosition = findViewById(R.id.edt_position);
        edtIntroduction = findViewById(R.id.edt_introduction);
        radioMale = findViewById(R.id.radio_male);
        radioFemale = findViewById(R.id.radio_female);
        radioOther = findViewById(R.id.radio_other);
        txtAppointments = findViewById(R.id.txt_appointments_today);
        txtPatients = findViewById(R.id.txt_manager_patients);

        mode = getIntent().getIntExtra("mode", 0);

        adapter = new AppointmentAdapter(this, items, this::onItemClick);
        adapter.setPatient(true);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        patientBedAdapter = new PatientBedAdapter(this, beds, null);

        recyclerView2 = findViewById(R.id.recycler_view_2);
        recyclerView2.setAdapter(patientBedAdapter);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        try {
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (me == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: " + me);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (this.mode == 1) {
            if (actionBar != null) {
                actionBar.setTitle(R.string.add_doctor);
            }

            edtID.setVisibility(View.GONE);
        } else if (this.mode == 2) {
            if (actionBar != null) {
                actionBar.setTitle(R.string.add_nurse);
            }

            edtID.setVisibility(View.GONE);
        } else {
            int staffID = getIntent().getIntExtra("staff_id", -1);

            if (staffID == -1) {
                Toast.makeText(StaffActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                StaffActivity.this.finish();
                return;
            }

            ApiService.apiService.getStaff(staffID, token).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (response.body() == null) {
                        Log.d(TAG, "onResponse: " + response);

                        Toast.makeText(StaffActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                        StaffActivity.this.finish();
                        return;
                    }

                    Log.d(TAG, "onResponse: " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        Toast.makeText(StaffActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        StaffActivity.this.finish();
                        return;
                    }

                    Gson gson = new Gson();
                    JsonObject object = response.body().get("results").getAsJsonObject();
                    staff = gson.fromJson(object, Staff.class);
                    updateStaff();

                    Type listType = new TypeToken<List<Appointment>>() {}.getType();
                    List<Appointment> appointments = gson.fromJson(object.get("list_appointment_today"), listType);
                    updateAppointments(appointments);

                    listType = new TypeToken<List<Bed>>() {}.getType();
                    List<Bed> beds = gson.fromJson(object.get("list_patient_in_bed"), listType);
                    Log.d(TAG, "onResponse: " + beds);
                    StaffActivity.this.beds.clear();
                    StaffActivity.this.beds.addAll(beds);

                    patientBedAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(StaffActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    StaffActivity.this.finish();
                }
            });
        }

        if (edtBirthday.getEditText() != null && me.getRoleID().equals("1")) {
            DatePickerDialog.OnDateSetListener date = (view, year, month, day) -> {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, day);

                updateLabel();
            };

            edtBirthday.getEditText().setOnClickListener(view -> {
                new DatePickerDialog(StaffActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            });
        }

        if (!me.getRoleID().equals("1")) {
            edtName.getEditText().setClickable(false);
            edtName.getEditText().setFocusable(false);
            edtName.getEditText().setFocusableInTouchMode(false);

            edtPhone.getEditText().setClickable(false);
            edtPhone.getEditText().setFocusable(false);
            edtPhone.getEditText().setFocusableInTouchMode(false);

            radioMale.setClickable(false);
            radioFemale.setClickable(false);
            radioOther.setClickable(false);

            edtAddress.getEditText().setClickable(false);
            edtAddress.getEditText().setFocusable(false);
            edtAddress.getEditText().setFocusableInTouchMode(false);

            edtPosition.getEditText().setClickable(false);
            edtPosition.getEditText().setFocusable(false);
            edtPosition.getEditText().setFocusableInTouchMode(false);

            edtIntroduction.getEditText().setClickable(false);
            edtIntroduction.getEditText().setFocusable(false);
            edtIntroduction.getEditText().setFocusableInTouchMode(false);

            edtPassword.getEditText().setClickable(false);
            edtPassword.getEditText().setFocusable(false);
            edtPassword.getEditText().setFocusableInTouchMode(false);
        }

        edtName.clearFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (me != null && me.getRoleID().equals("1")) {
            getMenuInflater().inflate(R.menu.menu_save, menu);
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.menu_save) {
            save();
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateAppointments(List<Appointment> appointments) {
        if (appointments == null) {
            return;
        }

        Collections.sort(appointments, (a1, a2) -> {
            int result = a1.getDate().compareTo(a2.getDate());
            if (result != 0) {
                return result;
            }

            return a1.getTime().compareTo(a2.getTime());
        });

        this.items.clear();
        String curDate = null;
        for (Appointment appointment : appointments) {
            this.items.add(appointment);
            Log.d(TAG, "updateAppointments: " + appointment);
        }

        adapter.notifyDataSetChanged();
    }

    private void onItemClick(Appointment appointment) {
        Log.d(TAG, "onItemClick: " + appointment);

        if (appointment != null) {
            Intent intent = new Intent(this, AppointmentActivity.class);
            intent.putExtra("appointment", appointment);
            intent.putExtra("mode", 1);
            startActivity(intent);
        }
    }

    private void updateStaff() {
        try {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle((staff.getRoleID().equals("2") ? R.string.doctor : R.string.nurse));
            }

            edtID.getEditText().setText(staff.getId());
            edtName.getEditText().setText(staff.getFullname());
            edtPhone.getEditText().setText(staff.getPhoneNumber());
            edtBirthday.getEditText().setText(staff.getBirthday());
            edtAddress.getEditText().setText(staff.getAddress());
            edtPosition.getEditText().setText(staff.getPosition());
            edtIntroduction.getEditText().setText(staff.getIntroduction());
            if (staff.getGender().equals("1")) {
                radioMale.setChecked(true);
            } else if (staff.getGender().equals("2")) {
                radioFemale.setChecked(true);
            } else {
                radioOther.setChecked(true);
            }

            if (!staff.getRoleID().equals("2")) {
                Log.d(TAG, "updateStaff: GONE");
                txtAppointments.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                txtPatients.setVisibility(View.GONE);
                recyclerView2.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.getDefault());
        edtBirthday.getEditText().setText(dateFormat.format(myCalendar.getTime()));
    }

    private void save() {
        if (edtPhone.getEditText() == null || edtPhone.getEditText().getText().toString().isEmpty() ||
                edtName.getEditText() == null || edtName.getEditText().getText().toString().isEmpty() ||
                edtAddress.getEditText() == null || edtAddress.getEditText().getText().toString().isEmpty() ||
                edtPosition.getEditText() == null || edtPosition.getEditText().getText().toString().isEmpty() ||
                edtIntroduction.getEditText() == null || edtIntroduction.getEditText().getText().toString().isEmpty() ||
                edtBirthday.getEditText() == null || edtBirthday.getEditText().getText().toString().isEmpty() ||
                edtBirthday.getEditText().getText().toString().equals(getString(R.string.date_format))) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mode != 0 && (edtPassword.getEditText() == null || edtPassword.getEditText().getText().toString().isEmpty())) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        String phone = edtPhone.getEditText().getText().toString();
        String name = edtName.getEditText().getText().toString();
        String birthday = edtBirthday.getEditText().getText().toString();
        String address = edtAddress.getEditText().getText().toString();
        String position = edtPosition.getEditText().getText().toString();
        String introduction = edtIntroduction.getEditText().getText().toString();
        int gender = (radioMale.isChecked() ? 1 : (radioFemale.isChecked() ? 2 : 3));

        int role;
        if (mode == 1) {
            role = 2;
        } else if (mode == 2) {
            role = 3;
        } else {
            role = Integer.parseInt(staff.getRoleID());
        }

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("fullname", gson.toJsonTree(name));
        jsonObject.add("number_phone", gson.toJsonTree(phone));
        jsonObject.add("gender", gson.toJsonTree(gender));
        jsonObject.add("birthday", gson.toJsonTree(birthday));
        jsonObject.add("address", gson.toJsonTree(address));
        jsonObject.add("position", gson.toJsonTree(position));
        jsonObject.add("introduction", gson.toJsonTree(introduction));
        jsonObject.add("is_staff", gson.toJsonTree(true));
        jsonObject.add("role", gson.toJsonTree(role));
        if (edtPassword.getEditText() != null && !edtPassword.getEditText().getText().toString().isEmpty()) {
            jsonObject.add("password", gson.toJsonTree(edtPassword.getEditText().getText().toString()));
        }

        if (mode != 0) {
            ApiService.apiService.createStaff(token, jsonObject).enqueue(callback);
        } else {
            ApiService.apiService.updateStaff(staff.getStaffID(), token, jsonObject).enqueue(callback);
        }
    }

    private final Callback<JsonObject> callback = new Callback<JsonObject>() {

        @Override
        public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
            if (response.code() == 401) {
                backToLogin();
                return;
            }

            if (response.body() == null) {
                Log.d(TAG, "onResponse: " + response);

                Toast.makeText(StaffActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                return;
            }

            if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                Toast.makeText(StaffActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                return;
            }

            Gson gson = new Gson();
            JsonObject object = response.body().get("results").getAsJsonObject();
            Staff staff = gson.fromJson(object, Staff.class);
            Log.d(TAG, "onResponse: " + mode + " " + staff);
            Intent result = new Intent();
            result.putExtra("data", staff);
            setResult(RESULT_OK, result);
            StaffActivity.this.finish();
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            t.printStackTrace();

            Toast.makeText(StaffActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
        }
    };

    private void backToLogin() {
        Toast.makeText(this, R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
    }
}