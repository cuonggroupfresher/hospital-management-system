package com.android.hospitalapp.activity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.hospitalapp.R;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.fragment.AppointmentFragment;
import com.android.hospitalapp.fragment.BedFragment;
import com.android.hospitalapp.fragment.ManagerFragment;
import com.android.hospitalapp.fragment.ScheduleFragment;
import com.android.hospitalapp.fragment.ScheduleV2Fragment;
import com.android.hospitalapp.model.NotificationModel;
import com.android.hospitalapp.model.Schedule;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.android.hospitalapp.service.SchedulingService;
import com.android.hospitalapp.util.AlarmUtils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private BottomNavigationView navigation;
    private FragmentManager fragmentManager;
    private ManagerFragment managerFragment;
    private ScheduleFragment scheduleFragment;
    private ScheduleV2Fragment scheduleV2Fragment;
    private BedFragment bedFragment;
    private AppointmentFragment appointmentFragment;
    private Fragment activeFragment;

    private Staff me;
    private String token;
    private boolean isList;

    private ActivityResultLauncher<Intent> mResultLauncher;

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::onRequestResult);

        AlarmUtils.clean(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                mResultLauncher.launch(intent);
            }
        }

        try {
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
            token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (me == null) {

            backToLogin();
            return;
        }
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }

        Log.d(TAG, "onCreate: " + me.getString());

        isList = true;

        navigation = findViewById(R.id.navigation);
        navigation.setOnItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager = getSupportFragmentManager();

        managerFragment = new ManagerFragment();
        scheduleFragment = new ScheduleFragment(this::onSwitchUI);
        scheduleV2Fragment = new ScheduleV2Fragment(this::onSwitchUI);
        bedFragment = new BedFragment();
        appointmentFragment = new AppointmentFragment();

        fragmentManager.beginTransaction().add(R.id.frame_container, managerFragment, "manager").commit();
        fragmentManager.beginTransaction().add(R.id.frame_container, scheduleV2Fragment, "schedulev2").hide(scheduleV2Fragment).commit();
        fragmentManager.beginTransaction().add(R.id.frame_container, scheduleFragment, "schedule").hide(scheduleFragment).commit();
        fragmentManager.beginTransaction().add(R.id.frame_container, bedFragment, "bed").hide(bedFragment).commit();
        fragmentManager.beginTransaction().add(R.id.frame_container, appointmentFragment, "appointment").hide(appointmentFragment).commit();

        activeFragment = managerFragment;

        if (me.getRoleID().equals("3")) {
            navigation.getMenu().removeItem(R.id.menu_appointment);
        }
        createNotificationChannel();
        if ("2".equals(me.getRoleID())) {
            getFutureScheduleDoctor();
        }
    }

    private static final String CHANNEL_ID = "TA22";

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void getFutureScheduleDoctor() {
        boolean isTesting = false;
        Log.d(TAG, "getFutureScheduleDoctor: ");

        if (isTesting) {
            Log.d(TAG, "getFutureScheduleDoctor: Testing - Push notification after 1 minutes");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.add(Calendar.MINUTE, 2);

            AlarmUtils.create(MainActivity.this, new NotificationModel(calendar, "Chạy thử hẹn giờ gửi thông báo sau 1 phút"), new NotificationModel(calendar1, "Chạy thử hẹn giờ gửi thông báo sau 2 phút"));
        } else {
            Gson gson = new Gson();
            JsonObject body = new JsonObject();
            body.add("number_phone", gson.toJsonTree(me.getPhoneNumber()));

            ApiService.apiService.getFutureSchedule(token, body).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (response.body() == null) {
                        Log.d(TAG, "onResponse: 1 " + response);
                        return;
                    }

                    Log.d(TAG, "onResponse: 2 " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        Log.d(TAG, "onResponse: 3 " + response.body().get("message").getAsString());
                        return;
                    }

                    JsonArray array = response.body().get("results").getAsJsonArray();
                    Type roomType = new TypeToken<List<Schedule>>() {}.getType();
                    List<Schedule> list = gson.fromJson(array, roomType);
                    Collections.sort(list, (a1, a2) -> {
                        return a1.getDate().compareTo(a2.getDate());
                    });

                    if (!list.isEmpty()) {
                        Calendar tomorrow = Calendar.getInstance();
                        tomorrow.add(Calendar.DATE, 1);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        String tomorrowStr = dateFormat.format(tomorrow.getTime());

                        List<NotificationModel> notificationModels = new ArrayList<>();

                        for (int i = 0; i < list.size(); i++) {
                            String notifyStr = "Bạn có lịch trực vào ngày mai";
                            String current = list.get(0).getDate();
                            Log.d(TAG, "onResponse: 4 " + current + " : " + tomorrowStr);

                            if (current.equals(tomorrowStr)) {
                                Log.d(TAG, "onResponse: 5");
                                sendNotification("Hospital App", notifyStr);
                            } else {
                                Log.d(TAG, "onResponse: 6");
                                try {
                                    String[] strings = current.split("-");

                                    Calendar time = Calendar.getInstance();
                                    time.set(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]) - 1, Integer.parseInt(strings[2]), 8, 0, 0);
                                    time.add(Calendar.DATE, -1);

                                    notificationModels.add(new NotificationModel(time, notifyStr));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        AlarmUtils.create(MainActivity.this, notificationModels);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private void sendNotification(String title, String content) {
        final Intent emptyIntent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, emptyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_foreground))
                .setContentTitle(title)
                .setContentText(content)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND)
                .setAutoCancel(true);
        builder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(1, builder.build());

        Toast.makeText(MainActivity.this, content, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (navigation.getSelectedItemId() != R.id.menu_manager) {
            navigation.setSelectedItemId(R.id.menu_manager);
            return;
        }

        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            System.exit(0);
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Nhấn trở về lần nữa để thoát", Toast.LENGTH_SHORT).show();

        new Handler(Looper.getMainLooper()).postDelayed(() -> doubleBackToExitPressedOnce = false, 1500);
    }

    private void onSwitchUI() {
        if (activeFragment instanceof ScheduleFragment) {
            fragmentManager.beginTransaction().hide(activeFragment).show(scheduleV2Fragment).commit();
            activeFragment = scheduleV2Fragment;
        } else if (activeFragment instanceof ScheduleV2Fragment) {
            fragmentManager.beginTransaction().hide(activeFragment).show(scheduleFragment).commit();
            activeFragment = scheduleFragment;
        }

        isList = !isList;
    }

    private void backToLogin() {
        Toast.makeText(this, R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
    }

    private final NavigationBarView.OnItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (item.getItemId() == R.id.menu_manager) {
                fragmentManager.beginTransaction().hide(activeFragment).show(managerFragment).commit();
                activeFragment = managerFragment;
                return true;
            } else if (item.getItemId() == R.id.menu_schedule) {
                if (isList) {
                    fragmentManager.beginTransaction().hide(activeFragment).show(scheduleV2Fragment).commit();
                    activeFragment = scheduleV2Fragment;
                } else {
                    fragmentManager.beginTransaction().hide(activeFragment).show(scheduleFragment).commit();
                    activeFragment = scheduleFragment;
                }
                return true;
            } else if (item.getItemId() == R.id.menu_bed) {
                fragmentManager.beginTransaction().hide(activeFragment).show(bedFragment).commit();
                activeFragment = bedFragment;
                return true;
            } else if (item.getItemId() == R.id.menu_appointment) {
                fragmentManager.beginTransaction().hide(activeFragment).show(appointmentFragment).commit();
                activeFragment = appointmentFragment;
                return true;
            } else {
                return false;
            }
        }
    };

    private void onRequestResult(ActivityResult result) {
        Log.d(TAG, "onRequestResult: " + result);
        if (result != null && result.getResultCode() == RESULT_CANCELED) {
            Toast.makeText(this, "Bạn có thể bỏ lỡ một số thông báo của chúng tôi khi ứng dụng đóng.", Toast.LENGTH_SHORT).show();
        }
    }
}
