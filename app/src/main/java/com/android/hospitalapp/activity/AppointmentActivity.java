package com.android.hospitalapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.hospitalapp.R;
import com.android.hospitalapp.adapter.PatientArrayAdapter;
import com.android.hospitalapp.adapter.StaffArrayAdapter;
import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Appointment;
import com.android.hospitalapp.model.Patient;
import com.android.hospitalapp.model.Staff;
import com.android.hospitalapp.service.ApiService;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentActivity extends AppCompatActivity {

    private static final String TAG = AppointmentActivity.class.getName();

    private static final Map<String, String> TIMES = new LinkedHashMap<String, String>() {{
        put("Ca 1 (08:00:00-09:00:00)", "1");
        put("Ca 2 (09:00:00-10:00:00)", "2");
        put("Ca 3 (10:00:00-11:00:00)", "3");
        put("Ca 4 (14:00:00-15:00:00)", "4");
        put("Ca 5 (15:00:00-16:00:00)", "5");
        put("Ca 6 (16:00:00-17:00:00)", "6");
    }};

    private AutoCompleteTextView edtPatient, edtDoctor, edtTime;
    private TextInputLayout edtDate, edtDetail;
    private RadioButton radioNew, radioRe;
    private SwitchCompat switchStatus;

    private final Calendar calendar = Calendar.getInstance();

    private Staff me, doctor;
    private Patient patient;
    private String token;
    private Appointment appointment;
    private int mode;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.appointment);
        }

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.PREF_ACCESS_TOKEN, "");
        if (token == null || token.isEmpty()) {
            backToLogin();
            return;
        } else if (!token.startsWith("Bearer ")) {
            token = "Bearer " + token;
        }
        Log.d(TAG, "onCreate: token " + token);

        try {
            me = new Gson().fromJson(sharedPreferences.getString(Constants.PREF_INFO, ""), Staff.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (me == null) {
            backToLogin();
            return;
        }

        Log.d(TAG, "onCreate: " + me);

        appointment = (Appointment) getIntent().getSerializableExtra("appointment");
        if (appointment == null) {
            Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }

        int mode = getIntent().getIntExtra("mode", 0);

        edtPatient = findViewById(R.id.edt_patient);
        edtDoctor = findViewById(R.id.edt_doctor);
        edtTime = findViewById(R.id.edt_time);
        edtDate = findViewById(R.id.edt_date);
        edtDetail = findViewById(R.id.edt_details);
        radioNew = findViewById(R.id.radio_new);
        radioRe = findViewById(R.id.radio_re);
        switchStatus = findViewById(R.id.switch_status);

        if (edtDate.getEditText() != null) {
            edtDate.getEditText().setText(appointment.getDate());

            if (mode == 0) {
                DatePickerDialog.OnDateSetListener date = (view, year, month, day) -> {
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, day);

                    updateLabel();
                };
                edtDate.getEditText().setOnClickListener(view -> {
                    new DatePickerDialog(this, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                });
            }
        }

        edtTime.setText("Ca " + appointment.getTime() + " (" + appointment.getTimeDisplay() + ")");
        if (mode == 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<>(TIMES.keySet()));
            edtTime.setAdapter(adapter);
        }

        switchStatus.setChecked(!appointment.isStatus());
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String today = format.format(calendar.getTime());
        if (appointment.getDate().compareTo(today) >= 0) {
            switchStatus.setEnabled(false);
        }

        if (appointment.isReExamination()) {
            radioRe.setChecked(true);
        } else {
            radioNew.setChecked(true);
        }
        if (mode == 1) {
            switchStatus.setClickable(false);
            radioNew.setClickable(false);
            radioRe.setClickable(false);

            edtDoctor.setClickable(false);
            edtDoctor.setFocusable(false);
            edtDoctor.setFocusableInTouchMode(false);

            edtPatient.setClickable(false);
            edtPatient.setFocusable(false);
            edtPatient.setFocusableInTouchMode(false);

            edtTime.setClickable(false);
            edtTime.setFocusable(false);
            edtTime.setFocusableInTouchMode(false);

            edtDetail.getEditText().setClickable(false);
            edtDetail.getEditText().setFocusable(false);
            edtDetail.getEditText().setFocusableInTouchMode(false);
        }

        edtDetail.getEditText().setText(appointment.getDescription());

        if (mode == 0) {
            getPatients();
            getDoctors();

            findViewById(R.id.btn_delete).setOnClickListener(this::delete);
            findViewById(R.id.btn_save).setOnClickListener(this::save);
        } else {
            edtDoctor.setText(appointment.getStaffIDUser() + " - " + appointment.getStaffName());
            edtPatient.setText(appointment.getPatientIDUser() + " - " + appointment.getPatientName());

            findViewById(R.id.btn_delete).setVisibility(View.GONE);
            findViewById(R.id.btn_save).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            AppointmentActivity.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getDoctors() {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("role_id", gson.toJsonTree(2));

        ApiService.apiService.getStaffs(token, jsonObject).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    AppointmentActivity.this.finish();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                    return;
                }

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Staff>>() {}.getType();

                List<Staff> list = new Gson().fromJson(json, listType);
                for (Staff staff : list) {
                    Log.d(TAG, "onResponse: " + staff);
                    if (staff.getStaffID() == appointment.getStaff()) {
                        doctor = staff;
                    }
                }

                if (doctor != null) {
                    edtDoctor.setText(doctor.toString());
                }

                StaffArrayAdapter adapter = new StaffArrayAdapter(AppointmentActivity.this, R.layout.simple_text_view, list);
                edtDoctor.setAdapter(adapter);
                edtDoctor.setOnItemClickListener((adapterView, view, position, l) -> {
                    if (adapterView.getItemAtPosition(position) instanceof Staff) {
                        doctor = (Staff) adapterView.getItemAtPosition(position);
                        Log.d(TAG, "onItemSelected: doctor " + doctor);
                    } else {
                        doctor = null;
                    }
                });
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                AppointmentActivity.this.finish();
            }
        });
    }

    private void getPatients() {
        ApiService.apiService.getPatients(token, new JsonObject()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    AppointmentActivity.this.finish();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                    return;
                }

                JsonElement json = response.body().get("results").getAsJsonObject().get("list");
                Type listType = new TypeToken<List<Patient>>() {}.getType();

                List<Patient> list = new Gson().fromJson(json, listType);
                for (Patient patient : list) {
                    Log.d(TAG, "onResponse: patient " + patient);
                    if (patient.getPatientID() == appointment.getPatient()) {
                        AppointmentActivity.this.patient = patient;
                    }
                }

                if (patient != null) {
                    edtPatient.setText(patient.toString());
                }

                PatientArrayAdapter adapter = new PatientArrayAdapter(AppointmentActivity.this, R.layout.simple_text_view, list);
                edtPatient.setAdapter(adapter);
                edtPatient.setOnItemClickListener((adapterView, view, position, l) -> {
                    if (adapterView.getItemAtPosition(position) instanceof Patient) {
                        patient = (Patient) adapterView.getItemAtPosition(position);
                        Log.d(TAG, "onItemSelected: patient " + patient);
                    } else {
                        patient = null;
                    }
                });
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                AppointmentActivity.this.finish();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.getDefault());
        edtDate.getEditText().setText(dateFormat.format(calendar.getTime()));
    }

    private void delete(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Xóa lịch khám");
        alert.setMessage("Bạn có chắc muốn xóa lịch khám?");
        alert.setPositiveButton(android.R.string.yes, (dialog, which) -> {
            Log.d(TAG, "delete: " + appointment.getId());
            JsonObject body = new JsonObject();
            body.add("id", new Gson().toJsonTree(appointment.getId()));

            Log.d(TAG, "delete: " + body);

            ApiService.apiService.removeAppointment(token, body).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 401) {
                        backToLogin();
                        return;
                    }

                    if (response.body() == null) {
                        Log.d(TAG, "onResponse: " + response);

                        Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                        AppointmentActivity.this.finish();
                        return;
                    }

                    Log.d(TAG, "onResponse: " + response.body());

                    if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                        Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                        return;
                    }

                    Toast.makeText(AppointmentActivity.this, "Xóa lịch khám thành công", Toast.LENGTH_SHORT).show();

                    Intent result = new Intent();
                    result.putExtra("data", appointment);
                    setResult(RESULT_OK, result);
                    AppointmentActivity.this.finish();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                }
            });
        });
        alert.setNegativeButton(android.R.string.no, (dialog, which) -> dialog.cancel());
        alert.show();
    }

    private void save(View view) {
        if (patient == null || doctor == null ||
                edtDate.getEditText() == null || edtDate.getEditText().getText().toString().isEmpty() ||
                edtTime.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        Gson gson = new Gson();
        JsonObject body = new JsonObject();
        body.add("id", gson.toJsonTree(appointment.getId()));
        body.add("patient", gson.toJsonTree(patient.getPatientID()));
        body.add("staff", gson.toJsonTree(doctor.getStaffID()));
        body.add("date", gson.toJsonTree(edtDate.getEditText().getText().toString()));
        body.add("time", gson.toJsonTree(TIMES.get(edtTime.getText().toString())));
        body.add("is_rexamination", gson.toJsonTree(radioRe.isChecked()));
        body.add("status", gson.toJsonTree(!switchStatus.isChecked()));
        if (edtDetail.getEditText() != null) {
            body.add("description", gson.toJsonTree(edtDetail.getEditText().getText().toString()));
        }

        Log.d(TAG, "save: " + body);

        ApiService.apiService.updateAppointment(token, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 401) {
                    backToLogin();
                    return;
                }

                if (response.body() == null) {
                    Log.d(TAG, "onResponse: " + response);

                    Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    AppointmentActivity.this.finish();
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Log.d(TAG, "onResponse: " + response.body().get("message").getAsString());
                    return;
                }

                Toast.makeText(AppointmentActivity.this, "Cập nhật lịch khám thành công", Toast.LENGTH_SHORT).show();

                Intent result = new Intent();
                result.putExtra("data", appointment);
                setResult(RESULT_OK, result);
                AppointmentActivity.this.finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(AppointmentActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void backToLogin() {
        Toast.makeText(this, R.string.error_unauthorized, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
    }
}