package com.android.hospitalapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.hospitalapp.constant.Constants;
import com.android.hospitalapp.model.Person;
import com.android.hospitalapp.service.ApiService;
import com.android.hospitalapp.R;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getName();

    private TextInputLayout edtPhone, edtPassword;
    private Button btnLogin;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtPhone = findViewById(R.id.edt_phone);
        edtPassword = findViewById(R.id.edt_password);
        btnLogin = findViewById(R.id.btn_login);

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String phone = sharedPreferences.getString(Constants.PREF_PHONE_NUMBER, "");
        if (edtPhone.getEditText() != null) {
            edtPhone.getEditText().setText(phone);
            edtPhone.getEditText().setSelection(phone.length());
            if (!phone.isEmpty()) {
                edtPassword.setFocusable(true);
            }
        }

        String pass = sharedPreferences.getString(Constants.PREF_PASSWORD, "");
        if (edtPassword.getEditText() != null) {
            edtPassword.getEditText().setText(pass);
        }

        btnLogin.setOnClickListener(this::onLoginClick);
    }

    private void onLoginClick(View view) {
        if (edtPhone.getEditText() == null || edtPhone.getEditText().getText().toString().isEmpty() ||
                edtPassword.getEditText() == null || edtPassword.getEditText().getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.fill_all, Toast.LENGTH_SHORT).show();
            return;
        }

        Pattern pattern = Pattern.compile("^[0]\\d{9}$");
        Matcher matcher = pattern.matcher(edtPhone.getEditText().getText().toString());
        if (!matcher.matches()) {
            Toast.makeText(this, "Số điện thoại không hợp lệ", Toast.LENGTH_SHORT).show();
            return;
        }

        btnLogin.setEnabled(false);

        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("number_phone", gson.toJsonTree(edtPhone.getEditText().getText().toString()));
        jsonObject.add("password", gson.toJsonTree(edtPassword.getEditText().getText().toString()));

        ApiService.apiService.login(jsonObject).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() == null) {
                    Toast.makeText(LoginActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    btnLogin.setEnabled(true);
                    return;
                }

                if (response.code() != 200 || response.body().get("code").getAsInt() != 200) {
                    Toast.makeText(LoginActivity.this, response.body().get("message").getAsString(), Toast.LENGTH_SHORT).show();
                    btnLogin.setEnabled(true);
                    return;
                }

                Log.d(TAG, "onResponse: " + response.body());

                SharedPreferences sharedPreferences = LoginActivity.this.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString(Constants.PREF_ACCESS_TOKEN, response.body().get("results").getAsJsonObject().get("access_token").getAsString());
                editor.putString(Constants.PREF_REFRESH_TOKEN, response.body().get("results").getAsJsonObject().get("refresh_token").getAsString());
                editor.putString(Constants.PREF_INFO, response.body().get("results").getAsJsonObject().get("info").getAsJsonObject().toString());
                editor.putString(Constants.PREF_PHONE_NUMBER, edtPhone.getEditText().getText().toString());
                editor.putString(Constants.PREF_PASSWORD, edtPassword.getEditText().getText().toString());

                editor.apply();

                Log.d(TAG, "onResponse: " + response.body().get("results").getAsJsonObject().get("access_token"));
                Log.d(TAG, "onResponse: " + response.body().get("results").getAsJsonObject().get("refresh_token"));
                Log.d(TAG, "onResponse: " + response.body().get("results").getAsJsonObject().get("info"));

                try {
                    Person person = new Gson().fromJson(response.body().get("results").getAsJsonObject().get("info").getAsJsonObject(), Person.class);
                    Log.d(TAG, "onResponse: " + person);

                    Intent intent;
                    if (person.isStaff()) {
                        intent = new Intent(LoginActivity.this, MainActivity.class);
                    } else {
                        intent = new Intent(LoginActivity.this, PatientActivity.class);
                    }
                    LoginActivity.this.startActivity(intent);
                    LoginActivity.this.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                    btnLogin.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(LoginActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                btnLogin.setEnabled(true);
            }
        });
    }
}